<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pt_PT" sourcelanguage="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="35"/>
        <source>&amp;About</source>
        <translation>&amp;Sobre</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="49"/>
        <location filename="../oscar/aboutdialog.cpp" line="129"/>
        <source>Release Notes</source>
        <translation>Notas da Versão</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="63"/>
        <source>Credits</source>
        <translation>Créditos</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="77"/>
        <source>GPL License</source>
        <translation>Licença GPL</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.ui" line="238"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="36"/>
        <source>Show data folder</source>
        <translation>Exibir pasta de dados</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="40"/>
        <source>About OSCAR %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="88"/>
        <source>Sorry, could not locate About file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="103"/>
        <source>Sorry, could not locate Credits file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="118"/>
        <source>Sorry, could not locate Release Notes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>OSCAR %1</source>
        <translation type="vanished">OSCAR %1</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="133"/>
        <source>Important:</source>
        <translation>Importante:</translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="134"/>
        <source>As this is a pre-release version, it is recommended that you &lt;b&gt;back up your data folder manually&lt;/b&gt; before proceeding, because attempting to roll back later may break things.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="147"/>
        <source>To see if the license text is available in your language, see %1.</source>
        <translation>Para ver se o texto da licença está disponível no seu idioma, consulte%1.</translation>
    </message>
</context>
<context>
    <name>CMS50F37Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="880"/>
        <source>Could not find the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.cpp" line="886"/>
        <source>Could not open the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CMS50Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="480"/>
        <source>Could not get data transmission from oximeter.</source>
        <translation>Impossível obter dados de transmissão do oxímetro.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="480"/>
        <source>Please ensure you select &apos;upload&apos; from the oximeter devices menu.</source>
        <translation>Por favor certifique-se de selecionar &apos;enviar&apos; do menu de dispositivos do oxímetro.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="548"/>
        <source>Could not find the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="554"/>
        <source>Could not open the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckUpdates</name>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="250"/>
        <source>Checking for newer OSCAR versions</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Daily</name>
    <message>
        <location filename="../oscar/daily.ui" line="506"/>
        <source>Go to the previous day</source>
        <translation>Ir para o dia anterior</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="551"/>
        <source>Show or hide the calender</source>
        <translation>Exibir ou ocultar o calendário</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="614"/>
        <source>Go to the next day</source>
        <translation>Ir para o próximo dia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="674"/>
        <source>Go to the most recent day with data records</source>
        <translation>Ir para o dia mais recente com dados registrados</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="861"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="923"/>
        <source>View Size</source>
        <translation>Ver tamanho</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="968"/>
        <location filename="../oscar/daily.ui" line="1391"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1022"/>
        <source>Journal</source>
        <translation>Diário</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1050"/>
        <source> i </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1106"/>
        <location filename="../oscar/daily.ui" line="1116"/>
        <source>Small</source>
        <translation>Pequeno</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1121"/>
        <source>Medium</source>
        <translation>Médio</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1126"/>
        <source>Big</source>
        <translation>Grande</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1196"/>
        <source>I&apos;m feeling ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1219"/>
        <source>If height is greater than zero in Preferences Dialog, setting weight here will show Body Mass Index (BMI) value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1492"/>
        <source>Flags</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1544"/>
        <source>Graphs</source>
        <translation>Gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1569"/>
        <source>Show/hide available graphs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1084"/>
        <source>Color</source>
        <translation>Cor</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1074"/>
        <source>u</source>
        <translation>u</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1062"/>
        <source>B</source>
        <translation>B</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1184"/>
        <source>Zombie</source>
        <translation>Zumbi</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1212"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1288"/>
        <source>Awesome</source>
        <translation>Ótimo</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1326"/>
        <source>B.M.I.</source>
        <translation>I.M.C.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1342"/>
        <source>Bookmarks</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1363"/>
        <source>Add Bookmark</source>
        <translation>Adicionar favorito</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1386"/>
        <source>Starts</source>
        <translation>Inícios</translation>
    </message>
    <message>
        <location filename="../oscar/daily.ui" line="1399"/>
        <source>Remove Bookmark</source>
        <translation>Remover Favorito</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="288"/>
        <source>Breakdown</source>
        <translation>Discriminação</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="288"/>
        <source>events</source>
        <translation>eventos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="632"/>
        <source>No %1 events are recorded this day</source>
        <translation>Nenhum evento %1 registrado neste dia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="752"/>
        <source>%1 event</source>
        <translation>evento %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="753"/>
        <source>%1 events</source>
        <translation>eventos %1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="300"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="301"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="796"/>
        <source>Session Start Times</source>
        <translation>Tempos de Início de Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="797"/>
        <source>Session End Times</source>
        <translation>Tempos de Término de Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1046"/>
        <source>Duration</source>
        <translation>Duração</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1032"/>
        <source>Position Sensor Sessions</source>
        <translation>Sessões de Sensor de Posição</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="183"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="364"/>
        <source>Time at Pressure</source>
        <translation>Tempo na Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1037"/>
        <source>Unknown Session</source>
        <translation>Sessão Desconhecida</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1066"/>
        <source>Click to %1 this session.</source>
        <translation>Clique para %1 essa sessão.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1066"/>
        <source>disable</source>
        <translation>ativar</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1066"/>
        <source>enable</source>
        <translation>desativar</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1082"/>
        <source>%1 Session #%2</source>
        <translation>%1 Sessão #%2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1083"/>
        <source>%1h %2m %3s</source>
        <translation>%1h %2m %3s</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1114"/>
        <source>&lt;b&gt;Please Note:&lt;/b&gt; All settings shown below are based on assumptions that nothing has changed since previous days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1276"/>
        <source>PAP Mode: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1280"/>
        <source>(Mode and Pressure settings missing; yesterday&apos;s shown.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>99.5%</source>
        <translation type="obsolete">90% {99.5%?}</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1416"/>
        <source>Time over leak redline</source>
        <translation>Tempo acima da linha vermelha de vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1491"/>
        <source>Event Breakdown</source>
        <translation>Discriminação de Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1504"/>
        <source>Unable to display Pie Chart on this system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1649"/>
        <source>10 of 10 Event Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1779"/>
        <source>Sessions all off!</source>
        <translation>Sessões todas desativadas!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1781"/>
        <source>Sessions exist for this day but are switched off.</source>
        <translation>Sessões existem para esse dia mas estão desativadas.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1784"/>
        <source>Impossibly short session</source>
        <translation>Sessão impossivelmente curta</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1785"/>
        <source>Zero hours??</source>
        <translation>Zero horas??</translation>
    </message>
    <message>
        <source>BRICK :(</source>
        <translation type="vanished">PROBLEMA :(</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1790"/>
        <source>Complain to your Equipment Provider!</source>
        <translation>Reclama para seu fornecedor do equipamento!</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2394"/>
        <source>This bookmark is in a currently disabled area..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2722"/>
        <source>10 of 10 Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1317"/>
        <source>Statistics</source>
        <translation>Estatísticas</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1244"/>
        <source>Oximeter Information</source>
        <translation>Informação do Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1249"/>
        <source>SpO2 Desaturations</source>
        <translation>Dessaturações de SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1250"/>
        <source>Pulse Change events</source>
        <translation>Eventos de Mudança de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1251"/>
        <source>SpO2 Baseline Used</source>
        <translation>Patamar SpO2 Usado</translation>
    </message>
    <message>
        <source>Machine Settings</source>
        <translation type="vanished">Configurações de Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1003"/>
        <source>Session Information</source>
        <translation>Informações da Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1023"/>
        <source>CPAP Sessions</source>
        <translation>Sessões CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1026"/>
        <source>Oximetry Sessions</source>
        <translation>Sessões de Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1029"/>
        <source>Sleep Stage Sessions</source>
        <translation>Sessões de Estátio de Sono</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1111"/>
        <source>Device Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1271"/>
        <source>Model %1 - %2</source>
        <translation>Modelo %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1396"/>
        <source>This day just contains summary data, only limited information is available.</source>
        <translation>Esse dia apenas contem dados sumários, apenas informação limitada está disponível.</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1406"/>
        <source>Total time in apnea</source>
        <translation>Tempo total em apneia</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1422"/>
        <source>Total ramp time</source>
        <translation>Tempo total de rampa</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1426"/>
        <source>Time outside of ramp</source>
        <translation>Tempo fora da rampa</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1467"/>
        <source>Start</source>
        <translation>Início</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1467"/>
        <source>End</source>
        <translation>Fim</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1708"/>
        <source>This CPAP device does NOT record detailed data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1788"/>
        <source>no data :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1789"/>
        <source>Sorry, this device only provides compliance data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1808"/>
        <source>&quot;Nothing&apos;s here!&quot;</source>
        <translation>&quot;Nada aqui!&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="1811"/>
        <source>No data is available for this day.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2107"/>
        <source>Pick a Colour</source>
        <translation>Escolha uma Cor</translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2437"/>
        <source>Bookmark at %1</source>
        <translation>Favorito em %1</translation>
    </message>
</context>
<context>
    <name>ExportCSV</name>
    <message>
        <location filename="../oscar/exportcsv.ui" line="14"/>
        <source>Export as CSV</source>
        <translation>Exportar como CSV</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="24"/>
        <source>Dates:</source>
        <translation>Datas:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="37"/>
        <source>Resolution:</source>
        <translation>Resolução:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="46"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="53"/>
        <source>Sessions</source>
        <translation>Sessões</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="60"/>
        <source>Daily</source>
        <translation>Diariamente</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="85"/>
        <source>Filename:</source>
        <translation>Arquivo:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="107"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="114"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="131"/>
        <source>Start:</source>
        <translation>Início:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="154"/>
        <source>End:</source>
        <translation>Fim:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="192"/>
        <source>Quick Range:</source>
        <translation>Intervalo Rápido:</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="200"/>
        <location filename="../oscar/exportcsv.cpp" line="61"/>
        <location filename="../oscar/exportcsv.cpp" line="123"/>
        <source>Most Recent Day</source>
        <translation>Dira Mais Recente</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="205"/>
        <location filename="../oscar/exportcsv.cpp" line="126"/>
        <source>Last Week</source>
        <translation>Última Semana</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="210"/>
        <location filename="../oscar/exportcsv.cpp" line="129"/>
        <source>Last Fortnight</source>
        <translation>Última Quinzena</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="215"/>
        <location filename="../oscar/exportcsv.cpp" line="132"/>
        <source>Last Month</source>
        <translation>Último Mês</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="220"/>
        <location filename="../oscar/exportcsv.cpp" line="135"/>
        <source>Last 6 Months</source>
        <translation>Últimos 6 Meses</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="225"/>
        <location filename="../oscar/exportcsv.cpp" line="138"/>
        <source>Last Year</source>
        <translation>Último Ano</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="230"/>
        <location filename="../oscar/exportcsv.cpp" line="120"/>
        <source>Everything</source>
        <translation>Tudo</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.ui" line="235"/>
        <location filename="../oscar/exportcsv.cpp" line="109"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="77"/>
        <source>Details_</source>
        <translation>Details_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="79"/>
        <source>Sessions_</source>
        <translation>Sessions_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="81"/>
        <source>Summary_</source>
        <translation>Summary_</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="88"/>
        <source>Select file to export to</source>
        <translation>Selecione arquivo para o qual exportar</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="89"/>
        <source>CSV Files (*.csv)</source>
        <translation>Arquivos CSV (*.csv)</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>DateTime</source>
        <translation>DataHora</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Session</source>
        <translation>Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Event</source>
        <translation>Evento</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="203"/>
        <source>Data/Duration</source>
        <translation>Data/Duração</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <source>Session Count</source>
        <translation>Contagem de Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>Start</source>
        <translation>Início</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="206"/>
        <location filename="../oscar/exportcsv.cpp" line="209"/>
        <source>End</source>
        <translation>Fim</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>Total Time</source>
        <translation>Tempo Total</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="207"/>
        <location filename="../oscar/exportcsv.cpp" line="210"/>
        <source>AHI</source>
        <translation>IAH</translation>
    </message>
    <message>
        <location filename="../oscar/exportcsv.cpp" line="214"/>
        <source> Count</source>
        <translation> Contagem</translation>
    </message>
</context>
<context>
    <name>FPIconLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="232"/>
        <source>Import Error</source>
        <translation>Erro de Importação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="233"/>
        <source>This device Record cannot be imported in this profile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation type="vanished">O relatório dessa máquina não pode ser importado nesse perfil.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.cpp" line="233"/>
        <source>The Day records overlap with already existing content.</source>
        <translation>Os registros diários sobrepõem-se com conteúdo pré-existente.</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="../oscar/help.ui" line="91"/>
        <source>Hide this message</source>
        <translation>Esconder essa mensagem</translation>
    </message>
    <message>
        <location filename="../oscar/help.ui" line="196"/>
        <source>Search Topic:</source>
        <translation>Tópico de Busca:</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="59"/>
        <source>Help Files are not yet available for %1 and will display in %2.</source>
        <translation>Arquivos de ajuda não estão disponíveis ainda para %1 e aparecerão em %2.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="69"/>
        <source>Help files do not appear to be present.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="85"/>
        <source>HelpEngine did not set up correctly</source>
        <translation>HelpEngine não instalou corretamente</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="100"/>
        <source>HelpEngine could not register documentation correctly.</source>
        <translation>HelpEngine não conseguiu registrar a documentação corretamente.</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="111"/>
        <source>Contents</source>
        <translation>Conteúdos</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="112"/>
        <source>Index</source>
        <translation>Índice</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="116"/>
        <source>Search</source>
        <translation>Busca</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="126"/>
        <source>No documentation available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="214"/>
        <source>Please wait a bit.. Indexing still in progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="240"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="241"/>
        <source>%1 result(s) for &quot;%2&quot;</source>
        <translation>%1 resultado(s) para &quot;%2&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/help.cpp" line="242"/>
        <source>clear</source>
        <translation>limpar</translation>
    </message>
</context>
<context>
    <name>MD300W1Loader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="166"/>
        <source>Could not find the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.cpp" line="172"/>
        <source>Could not open the oximeter file:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../oscar/mainwindow.ui" line="534"/>
        <source>&amp;Statistics</source>
        <translation>E&amp;statísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="588"/>
        <source>Report Mode</source>
        <translation>Modo Relatório</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="595"/>
        <location filename="../oscar/mainwindow.ui" line="2890"/>
        <source>Standard</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="605"/>
        <source>Monthly</source>
        <translation>Mensal</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="612"/>
        <source>Date Range</source>
        <translation>Faixa de Data</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="983"/>
        <source>Statistics</source>
        <translation>Estatísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1033"/>
        <source>Daily</source>
        <translation>Diariamente</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1077"/>
        <source>Overview</source>
        <translation>Visão Geral</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1121"/>
        <source>Oximetry</source>
        <translation>Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1171"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1221"/>
        <source>Help</source>
        <translation>Ajuda</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2397"/>
        <source>&amp;File</source>
        <translation>&amp;Arquivo</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2422"/>
        <source>&amp;View</source>
        <translation>&amp;Exibir</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2426"/>
        <source>&amp;Reset Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2451"/>
        <source>&amp;Help</source>
        <translation>A&amp;juda</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2455"/>
        <source>Troubleshooting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2481"/>
        <source>&amp;Data</source>
        <translation>&amp;Dados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2485"/>
        <source>&amp;Advanced</source>
        <translation>A&amp;vançado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2517"/>
        <source>Rebuild CPAP Data</source>
        <translation>Recompilar Dados CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2540"/>
        <source>&amp;Import CPAP Card Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2566"/>
        <source>Show Daily view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2577"/>
        <source>Show Overview view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2623"/>
        <source>&amp;Maximize Toggle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2626"/>
        <source>Maximize window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2645"/>
        <source>Reset Graph &amp;Heights</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2648"/>
        <source>Reset sizes of graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2708"/>
        <source>Show Right Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2722"/>
        <source>Show Statistics view</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2735"/>
        <source>Import &amp;Dreem Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2778"/>
        <source>Show &amp;Line Cursor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2800"/>
        <source>Show Daily Left Sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2817"/>
        <source>Show Daily Calendar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2838"/>
        <source>Create zip of CPAP data card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2843"/>
        <source>Create zip of OSCAR diagnostic logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2848"/>
        <source>Create zip of all OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2863"/>
        <source>Report an Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2868"/>
        <source>System Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2879"/>
        <source>Show &amp;Pie Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2882"/>
        <source>Show Pie Chart on Daily page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2893"/>
        <source>Standard graph order, good for CPAP, APAP, Bi-Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2898"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2901"/>
        <source>Advanced graph order, good for ASV, AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2912"/>
        <source>Show Personal Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2920"/>
        <source>Check For &amp;Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2928"/>
        <source>Purge Current Selected Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2933"/>
        <source>&amp;CPAP</source>
        <translation type="unfinished">&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2938"/>
        <source>&amp;Oximetry</source>
        <translation type="unfinished">&amp;Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2943"/>
        <source>&amp;Sleep Stage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2948"/>
        <source>&amp;Position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2953"/>
        <source>&amp;All except Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2958"/>
        <source>All including &amp;Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2548"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Preferências</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2553"/>
        <source>&amp;Profiles</source>
        <translation>&amp;Perfis</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2615"/>
        <source>&amp;About OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2833"/>
        <source>Show Performance Information</source>
        <translation>Mostrar Informação de Desempenho</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2853"/>
        <source>CSV Export Wizard</source>
        <translation>Assistente de Exportação CSV</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2858"/>
        <source>Export for Review</source>
        <translation>Exportar para Revisão</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="110"/>
        <source>E&amp;xit</source>
        <translation>Sai&amp;r</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2558"/>
        <source>Exit</source>
        <translation>Sair</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2563"/>
        <source>View &amp;Daily</source>
        <translation>Mostrar &amp;Diariamente</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2574"/>
        <source>View &amp;Overview</source>
        <translation>Ver Visã&amp;o Geral</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2585"/>
        <source>View &amp;Welcome</source>
        <translation>Ver Boas-vi&amp;ndas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2610"/>
        <source>Use &amp;AntiAliasing</source>
        <translation>Usar &amp;AntiAliasing</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2637"/>
        <source>Show Debug Pane</source>
        <translation>Mostrar Painel Depurador</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2653"/>
        <source>Take &amp;Screenshot</source>
        <translation>Salvar Captura de &amp;Tela</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2661"/>
        <source>O&amp;ximetry Wizard</source>
        <translation>Assistente de O&amp;ximetria</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2669"/>
        <source>Print &amp;Report</source>
        <translation>Imprimir &amp;Relatório</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2674"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Editar Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2765"/>
        <source>Import &amp;Viatom/Wellue Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2814"/>
        <source>Daily Calendar</source>
        <translation>Calendário Diário</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2825"/>
        <source>Backup &amp;Journal</source>
        <translation>Fazer Backu&amp;p do Diário</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2679"/>
        <source>Online Users &amp;Guide</source>
        <translation>&amp;Guia Online do Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2684"/>
        <source>&amp;Frequently Asked Questions</source>
        <translation>Perguntas &amp;Frequentes</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2689"/>
        <source>&amp;Automatic Oximetry Cleanup</source>
        <translation>Limpeza &amp;Automática de Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2694"/>
        <source>Change &amp;User</source>
        <translation>Trocar &amp;Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2500"/>
        <source>Purge &amp;Current Selected Day</source>
        <translation>Remover Dia S&amp;elecionado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2705"/>
        <source>Right &amp;Sidebar</source>
        <translation>Barra Lateral Di&amp;reita</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2797"/>
        <source>Daily Sidebar</source>
        <translation>Barra Lateral Diária</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2716"/>
        <source>View S&amp;tatistics</source>
        <translation>Ver E&amp;statísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="912"/>
        <source>Navigation</source>
        <translation>Navegação</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="1343"/>
        <source>Bookmarks</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2338"/>
        <source>Records</source>
        <translation>Registros</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2401"/>
        <source>Exp&amp;ort Data</source>
        <translation>Exp&amp;ortar Dados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="939"/>
        <source>Profiles</source>
        <translation>Perfis</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2489"/>
        <source>Purge Oximetry Data</source>
        <translation>Remover Dados Oximétricos</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2495"/>
        <source>Purge ALL Device Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2719"/>
        <source>View Statistics</source>
        <translation>Ver Estatísticas</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2730"/>
        <source>Import &amp;ZEO Data</source>
        <translation>Importar Dados &amp;ZEO</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2740"/>
        <source>Import RemStar &amp;MSeries Data</source>
        <translation>Importar Dados RemStar &amp;MSeries</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2745"/>
        <source>Sleep Disorder Terms &amp;Glossary</source>
        <translation>G&amp;lossário de Termos de Desordens do Sono</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2750"/>
        <source>Change &amp;Language</source>
        <translation>A&amp;lterar Idioma</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2755"/>
        <source>Change &amp;Data Folder</source>
        <translation>Alterar Pasta de &amp;Dados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2760"/>
        <source>Import &amp;Somnopose Data</source>
        <translation>Importar Dados &amp;Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.ui" line="2770"/>
        <source>Current Days</source>
        <translation>Dias Atuais</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="540"/>
        <location filename="../oscar/mainwindow.cpp" line="2200"/>
        <source>Welcome</source>
        <translation>Bem-Vindo</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="107"/>
        <source>&amp;About</source>
        <translation>So&amp;bre</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1001"/>
        <source>Access to Import has been blocked while recalculations are in progress.</source>
        <translation>Acesso para importar foi bloqueado enquanto cálculos estão em progresso.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1162"/>
        <source>Importing Data</source>
        <translation>Importando Dados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="810"/>
        <location filename="../oscar/mainwindow.cpp" line="1876"/>
        <source>Please wait, importing from backup folder(s)...</source>
        <translation>Por favor aguarde, importando da(s) pasta(s) de backup...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="746"/>
        <source>Import Problem</source>
        <translation>Importar Problema</translation>
    </message>
    <message>
        <source>Couldn&apos;t find any valid Machine Data at

%1</source>
        <translation type="vanished">Impossível encontrar qualquer Dado de Máquina valido em

%1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="911"/>
        <source>Please insert your CPAP data card...</source>
        <translation>Por favor insira seu cartão de dados do CPAP...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1005"/>
        <source>Import is already running in the background.</source>
        <translation>Importação já em execução em segundo plano.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1053"/>
        <source>CPAP Data Located</source>
        <translation>Dados do CPAP Localizados</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1088"/>
        <source>Import Reminder</source>
        <translation>Lembrete de Importação</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1353"/>
        <source>Please open a profile first.</source>
        <translation>Por favor abra um perfil primeiro.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1415"/>
        <source>Check for updates not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1851"/>
        <source>Are you sure you want to rebuild all CPAP data for the following device:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1854"/>
        <source>Please note, that this could result in loss of data if OSCAR&apos;s backups have been disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2347"/>
        <location filename="../oscar/mainwindow.cpp" line="2351"/>
        <source>There was a problem opening %1 Data File: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2350"/>
        <source>%1 Data Import of %2 file(s) complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2352"/>
        <source>%1 Import Partial Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2354"/>
        <source>%1 Data Import complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2569"/>
        <source>%1&apos;s Journal</source>
        <translation>Diário de %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2571"/>
        <source>Choose where to save journal</source>
        <translation>Escolha onde salvar o diário</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2571"/>
        <source>XML Files (*.xml)</source>
        <translation>Arquivos XML (*.xml)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1358"/>
        <source>Access to Preferences has been blocked until recalculation completes.</source>
        <translation>Acesso às preferêcias foi bloqueado até que os cálculos terminem.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2483"/>
        <source>Are you sure you want to delete oximetry data for %1</source>
        <translation>Você tem certeza de que deseja deletar dados oximétricos para %1</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2485"/>
        <source>&lt;b&gt;Please be aware you can not undo this operation!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Por favor esteja ciente de que você não pode desfazer a operação!&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2515"/>
        <source>Select the day with valid oximetry data in daily view first.</source>
        <translation>Selecione o dia com dados oxímetros válidos na visualização diária primeiro.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="268"/>
        <source>Help Browser</source>
        <translation>Navegador de Ajuda</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="522"/>
        <source>Loading profile &quot;%1&quot;</source>
        <translation>Carregando perfil &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="918"/>
        <source>Choose a folder</source>
        <translation>Escolha uma pasta</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1046"/>
        <source>A %1 file structure for a %2 was located at:</source>
        <translation>Uma estrutura de arquivos %1 para %2 foi localizada em:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1048"/>
        <source>A %1 file structure was located at:</source>
        <translation>Uma estrutura de arquivos %1 foi localizada em:</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1013"/>
        <source>Would you like to import from this location?</source>
        <translation>Você gostaria de importar dessa localização?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1057"/>
        <source>Specify</source>
        <translation>Especifique</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1474"/>
        <source>There was an error saving screenshot to file &quot;%1&quot;</source>
        <translation>Erro ao salvar a captura de tela para o arquivo &quot;%1&quot;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1476"/>
        <source>Screenshot saved to file &quot;%1&quot;</source>
        <translation>Captura de tela salva para o arquivo &quot;%1&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1861"/>
        <source>For some reason, OSCAR does not have any backups for the following device:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1863"/>
        <source>Provided you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your CPAP data&lt;/i&gt;, you can still complete this operation, but you will have to restore from your backups manually.</source>
        <translation>Contanto que você possua &lt;i&gt;seus&lt;b&gt;próprios&lt;/b&gt; backups para TODOS os seus dados de CPAP&lt;/i&gt;, você ainda pode completar essa operação, mas você precisará restaurar manualmente a partir dos seus backups.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1864"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Tem certeza de que deseja fazer isso?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1879"/>
        <source>Because there are no internal backups to rebuild from, you will have to restore from your own.</source>
        <translation>Por não existir backups internos a partir dos quais recompilar, você terá que restaurar a partir dos seus próprios.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="742"/>
        <source>Imported %1 CPAP session(s) from

%2</source>
        <translation>Importadas %1 sessão(ões) CPAP de

%2</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="561"/>
        <source>%1 (Profile: %2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="742"/>
        <source>Import Success</source>
        <translation>Sucesso na Importação</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="744"/>
        <source>Already up to date with CPAP data at

%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="744"/>
        <source>Up to date</source>
        <translation>Atualizado</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="746"/>
        <source>Couldn&apos;t find any valid Device Data at

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="997"/>
        <source>No profile has been selected for Import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1087"/>
        <source>Please remember to select the root folder or drive letter of your data card, and not a folder inside it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1103"/>
        <source>Find your CPAP data card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1464"/>
        <source>Choose where to save screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1464"/>
        <source>Image files (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1538"/>
        <source>The User&apos;s Guide will open in your default browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1546"/>
        <source>The FAQ is not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1688"/>
        <location filename="../oscar/mainwindow.cpp" line="1715"/>
        <source>If you can read this, the restart command didn&apos;t work. You will have to do it yourself manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Would you like to import from your own backups now? (you will have no data visible for this machine until you do)</source>
        <translation type="vanished">Gostaria de importar de seus próprios backups agora? (você não terá quaisquer dados visíveis para essa máquina até fazê-lo)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2259"/>
        <source>The Glossary will open in your default browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2591"/>
        <source>Export review is not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2601"/>
        <source>Would you like to zip this card?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2623"/>
        <location filename="../oscar/mainwindow.cpp" line="2694"/>
        <location filename="../oscar/mainwindow.cpp" line="2745"/>
        <source>Choose where to save zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2623"/>
        <location filename="../oscar/mainwindow.cpp" line="2694"/>
        <location filename="../oscar/mainwindow.cpp" line="2745"/>
        <source>ZIP files (*.zip)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2670"/>
        <location filename="../oscar/mainwindow.cpp" line="2708"/>
        <location filename="../oscar/mainwindow.cpp" line="2779"/>
        <source>Creating zip...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2655"/>
        <location filename="../oscar/mainwindow.cpp" line="2763"/>
        <source>Calculating size...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2801"/>
        <source>Reporting issues is not yet implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1929"/>
        <source>Note as a precaution, the backup folder will be left in place.</source>
        <translation>Note como precaução, a pasta de backup será mantida no mesmo lugar.</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1880"/>
        <source>Would you like to import from your own backups now? (you will have no data visible for this device until you do)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1931"/>
        <source>OSCAR does not have any backups for this device!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1932"/>
        <source>Unless you have made &lt;i&gt;your &lt;b&gt;own&lt;/b&gt; backups for ALL of your data for this device&lt;/i&gt;, &lt;font size=+2&gt;you will lose this device&apos;s data &lt;b&gt;permanently&lt;/b&gt;!&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1938"/>
        <source>You are about to &lt;font size=+2&gt;obliterate&lt;/font&gt; OSCAR&apos;s device database for the following device:&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1941"/>
        <source>Are you &lt;b&gt;absolutely sure&lt;/b&gt; you want to proceed?</source>
        <translation>Você tem &lt;b&gt;absoluta certeza&lt;/b&gt; de que deseja prosseguir?</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="1996"/>
        <source>A file permission error casued the purge process to fail; you will have to delete the following folder manually:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2046"/>
        <source>No help is available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2244"/>
        <source>There was a problem opening MSeries block File: </source>
        <translation>Houve um problema abrindo o arquivo de bloco MSeries: </translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2248"/>
        <source>MSeries Import complete</source>
        <translation>Importação de MSeries completa</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2810"/>
        <source>OSCAR Information</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MinMaxWidget</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2021"/>
        <source>Auto-Fit</source>
        <translation>Auto-Ajuste</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2022"/>
        <source>Defaults</source>
        <translation>Padrões</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2023"/>
        <source>Override</source>
        <translation>Substituir</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2024"/>
        <source>The Y-Axis scaling mode, &apos;Auto-Fit&apos; for automatic scaling, &apos;Defaults&apos; for settings according to manufacturer, and &apos;Override&apos; to choose your own.</source>
        <translation>O modo de escala do eixo-Y, &apos;Auto-Ajuste&apos; para auto-escala, &apos;Padrões&apos; para configurar de acordo com o fabricante, e &apos;Substituir&apos; para escolher o seu próprio.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2030"/>
        <source>The Minimum Y-Axis value.. Note this can be a negative number if you wish.</source>
        <translation>O valor Mínimo de eixo-Y.. Note que esse pode ser um número negativo se você quiser.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2031"/>
        <source>The Maximum Y-Axis value.. Must be greater than Minimum to work.</source>
        <translation>O valor Máximo de eixo-Y.. Deve ser maior do que o mínimo para funcionar.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2066"/>
        <source>Scaling Mode</source>
        <translation>Modo de Escala</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2088"/>
        <source>This button resets the Min and Max to match the Auto-Fit</source>
        <translation>Esse botão redefine o Min e Máx para combinar com a Auto-Escala</translation>
    </message>
</context>
<context>
    <name>NewProfile</name>
    <message>
        <location filename="../oscar/newprofile.ui" line="14"/>
        <source>Edit User Profile</source>
        <translation>Editar Perfil de Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="73"/>
        <source>I agree to all the conditions above.</source>
        <translation>Eu concordo com todas as condições acima.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="114"/>
        <source>User Information</source>
        <translation>Informação de Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="140"/>
        <source>User Name</source>
        <translation>Nome de Utilizador</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="158"/>
        <source>Password Protect Profile</source>
        <translation>Proteger Perfil com Senha</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="185"/>
        <source>Password</source>
        <translation>Senha</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="199"/>
        <source>...twice...</source>
        <translation>...duas vezes...</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="216"/>
        <source>Locale Settings</source>
        <translation>Configurações de Localidade</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="288"/>
        <source>Country</source>
        <translation>País</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="256"/>
        <source>TimeZone</source>
        <translation>Zona Horária</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="54"/>
        <source>about:blank</source>
        <translation>about:blank</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="155"/>
        <source>Very weak password protection and not recommended if security is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="243"/>
        <source>DST Zone</source>
        <translation>Horário de Verão</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="323"/>
        <source>Personal Information (for reports)</source>
        <translation>Informação Pessoal (para relatórios)</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="347"/>
        <source>First Name</source>
        <translation>Primeiro Nome</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="357"/>
        <source>Last Name</source>
        <translation>Sobrenome</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="367"/>
        <source>It&apos;s totally ok to fib or skip this, but your rough age is needed to enhance accuracy of certain calculations.</source>
        <translation>Não há problema em mentir ou pular isso, mas sua idade aproximada é necessária para melhorar a qualidade de certos cálculos.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="370"/>
        <source>D.O.B.</source>
        <translation>Data de Nasc.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="386"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Biological (birth) gender is sometimes needed to enhance the accuracy of a few calculations, feel free to leave this blank and skip any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Sexo biológico (nascença) é necessário algumas vezes para melhorar a precisão de alguns cálculos, sinta-se livre para deixar isso em branco e pular perguntas.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="389"/>
        <source>Gender</source>
        <translation>Sexo</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="402"/>
        <source>Male</source>
        <translation>Masculino</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="407"/>
        <source>Female</source>
        <translation>Feminino</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="423"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="462"/>
        <source>Metric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="467"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="480"/>
        <source>Contact Information</source>
        <translation>Informações de Contato</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="507"/>
        <location filename="../oscar/newprofile.ui" line="782"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="524"/>
        <location filename="../oscar/newprofile.ui" line="813"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="534"/>
        <location filename="../oscar/newprofile.ui" line="803"/>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="579"/>
        <source>CPAP Treatment Information</source>
        <translation>Informação de Tratamento CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="606"/>
        <source>Date Diagnosed</source>
        <translation>Data do Diagnóstico</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="620"/>
        <source>Untreated AHI</source>
        <translation>IAH Não Tratado</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="634"/>
        <source>CPAP Mode</source>
        <translation>Modo CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="642"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="647"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="652"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="657"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="665"/>
        <source>RX Pressure</source>
        <translation>Pressão RX</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="711"/>
        <source>Doctors / Clinic Information</source>
        <translation>Informação do Médico / Clínica</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="738"/>
        <source>Doctors Name</source>
        <translation>Nome do Médico</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="755"/>
        <source>Practice Name</source>
        <translation>Nome da Clínica</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="765"/>
        <source>Patient ID</source>
        <translation>RG do Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="957"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancel</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="973"/>
        <source>&amp;Back</source>
        <translation>&amp;Voltar</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.ui" line="989"/>
        <location filename="../oscar/newprofile.cpp" line="279"/>
        <location filename="../oscar/newprofile.cpp" line="288"/>
        <source>&amp;Next</source>
        <translation>&amp;Próximo</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="63"/>
        <source>Select Country</source>
        <translation>Selecione o País</translation>
    </message>
    <message>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP machines and related equipment.</source>
        <translation type="vanished">Esse software está sendo projetado para ajudá-lo a revisar os dados produzidos por máquinas CPAP e equipamento relacionado.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="118"/>
        <source>PLEASE READ CAREFULLY</source>
        <translation>POR FAVOR LEIA COM ATENÇÃO</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="122"/>
        <source>Accuracy of any data displayed is not and can not be guaranteed.</source>
        <translation>Precisão de qualquer dado mostrado não é não pode ser garantida.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="124"/>
        <source>Any reports generated are for PERSONAL USE ONLY, and NOT IN ANY WAY fit for compliance or medical diagnostic purposes.</source>
        <translation>Quaisquer relatórios gerados são para USO PESSOAL SOMENTE, e DE NENHUM MODO servem para propósitos de observância ou diagnóstico médico.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="131"/>
        <source>Use of this software is entirely at your own risk.</source>
        <translation>Uso desse software é interamente por sua conta e risco.</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="134"/>
        <source>OSCAR is copyright &amp;copy;2011-2018 Mark Watkins and portions &amp;copy;2019-2022 The OSCAR Team</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="111"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="113"/>
        <source>This software is being designed to assist you in reviewing the data produced by your CPAP Devices and related equipment.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="116"/>
        <source>OSCAR has been released freely under the &lt;a href=&apos;qrc:/COPYING&apos;&gt;GNU Public License v3&lt;/a&gt;, and comes with no warranty, and without ANY claims to fitness for any purpose.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="119"/>
        <source>OSCAR is intended merely as a data viewer, and definitely not a substitute for competent medical guidance from your Doctor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="127"/>
        <source>The authors will not be held liable for &lt;u&gt;anything&lt;/u&gt; related to the use or misuse of this software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="157"/>
        <source>Please provide a username for this profile</source>
        <translation>Por favor forneça um nome de utilizador para esse perfil</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="167"/>
        <source>Passwords don&apos;t match</source>
        <translation>Senhas não correspondem</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Profile Changes</source>
        <translation>Mudanças de Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="197"/>
        <source>Accept and save this information?</source>
        <translation>Aceitar e salvar essa informação?</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="277"/>
        <source>&amp;Finish</source>
        <translation>&amp;Finalizar</translation>
    </message>
    <message>
        <location filename="../oscar/newprofile.cpp" line="455"/>
        <source>&amp;Close this window</source>
        <translation>Fe&amp;char essa janela</translation>
    </message>
</context>
<context>
    <name>Overview</name>
    <message>
        <location filename="../oscar/overview.ui" line="68"/>
        <source>Range:</source>
        <translation>Intervalo:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="82"/>
        <source>Last Week</source>
        <translation>Última Semana</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="87"/>
        <source>Last Two Weeks</source>
        <translation>Últimas Duas Semanas</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="92"/>
        <source>Last Month</source>
        <translation>Último Mês</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="97"/>
        <source>Last Two Months</source>
        <translation>Últimos Dois Meses</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="102"/>
        <source>Last Three Months</source>
        <translation>Últimos Três Meses</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="107"/>
        <source>Last 6 Months</source>
        <translation>Últimos 6 Meses</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="112"/>
        <source>Last Year</source>
        <translation>Último Ano</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="117"/>
        <source>Everything</source>
        <translation>Tudo</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="122"/>
        <source>Custom</source>
        <translation>Personalizado</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="127"/>
        <source>Snapshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="135"/>
        <source>Start:</source>
        <translation>Início:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="158"/>
        <source>End:</source>
        <translation>Fim:</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="181"/>
        <source>Reset view to selected date range</source>
        <translation>Redefinir visualização para a faixa de data selecionada</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="230"/>
        <source>Toggle Graph Visibility</source>
        <translation>Alternar Visibilidade do Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="265"/>
        <source>Drop down to see list of graphs to switch on/off.</source>
        <translation>Desça para ver a lista de gráficos para des/ativar.</translation>
    </message>
    <message>
        <location filename="../oscar/overview.ui" line="272"/>
        <source>Graphs</source>
        <translation>Gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="261"/>
        <source>Respiratory
Disturbance
Index</source>
        <translation>Índice
Distúrbio
Respiratório</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="263"/>
        <source>Apnea
Hypopnea
Index</source>
        <translation>Índice
Hipoapneia
Apneia</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="270"/>
        <source>Usage</source>
        <translation>Uso</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="270"/>
        <source>Usage
(hours)</source>
        <translation>Uso
(horas)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="274"/>
        <source>Session Times</source>
        <translation>Tempos de Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="283"/>
        <source>Total Time in Apnea</source>
        <translation>Tempo Total em Apneia</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="283"/>
        <source>Total Time in Apnea
(Minutes)</source>
        <translation>Tempo Total em Apneia
(Minutos)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="335"/>
        <source>Body
Mass
Index</source>
        <translation>Índice
Massa
Corporal</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="341"/>
        <source>How you felt
(0-10)</source>
        <translation>Como se sentiu
(0-10)</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="463"/>
        <source>10 of 10 Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="803"/>
        <source>Show all graphs</source>
        <translation>Mostrar todos os gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="816"/>
        <source>Hide all graphs</source>
        <translation>Esconder todos os gráficos</translation>
    </message>
</context>
<context>
    <name>OximeterImport</name>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="61"/>
        <location filename="../oscar/oximeterimport.cpp" line="38"/>
        <source>Oximeter Import Wizard</source>
        <translation>Assistente de Importação do Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="444"/>
        <source>Skip this page next time.</source>
        <translation>Pular essa página da próxima vez.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="499"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Please note: &lt;/span&gt;&lt;span style=&quot; font-style:italic;&quot;&gt;First select your correct oximeter type from the pull-down menu below.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="542"/>
        <source>Where would you like to import from?</source>
        <translation>De onde você gostaria de importar?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="586"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:12pt; font-weight:700;&quot;&gt;FIRST Select your Oximeter from these groups:&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="648"/>
        <source>CMS50E/F users, when importing directly, please don&apos;t select upload on your device until OSCAR prompts you to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="685"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If enabled, OSCAR will automatically reset your CMS50&apos;s internal clock using your computers current time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="717"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Here you can enter a 7 character name for this oximeter.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="758"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option will erase the imported session from your oximeter after import has completed. &lt;/p&gt;&lt;p&gt;Use with caution,  because if something goes wrong before OSCAR saves your session, you can&apos;t get it back.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="787"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import (via cable) from your oximeters internal recordings.&lt;/p&gt;&lt;p&gt;After selecting on this option, old Contec oximeters will require you to use the device&apos;s menu to initiate the upload.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="829"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;If you don&apos;t mind a being attached to a running computer overnight, this option provide a useful plethysomogram graph, which gives an indication of heart rhythm, on top of the normal oximetry readings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Se você não se importa de ficar conectado a um computador ligado a noite toda, essa opção fornece um gráfico pletismográfico útil, que fornece uma indicação de rítmo cardíaco, em adição às leituras oximétricas normais.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="835"/>
        <source>Record attached to computer overnight (provides plethysomogram)</source>
        <translation>Registrar conectado ao computador durante a noite (fornece pletismograma)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="868"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This option allows you to import from data files created by software that came with your Pulse Oximeter, such as SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Essa opção permite que você importe arquivos de dado criados pelo software que acompanha o seu Oxímetro de Pulso, como o SpO2Review.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="874"/>
        <source>Import from a datafile saved by another program, like SpO2Review</source>
        <translation>Importar de um arquivo de dado salvo por outro programa, como SpO2Review</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="953"/>
        <source>Please connect your oximeter device</source>
        <translation>Por favor conecte seu aparelho oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="971"/>
        <source>If you can read this, you likely have your oximeter type set wrong in preferences.</source>
        <translation>Se você pode ler isso, provavelmente você configurou o tipo de oxímetro errado nas preferências.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1000"/>
        <source>Please connect your oximeter device, turn it on, and enter the menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1026"/>
        <source>Press Start to commence recording</source>
        <translation>Pressione Início para começar o registro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1071"/>
        <source>Show Live Graphs</source>
        <translation>Mostrar Gráficos em Tempo Real</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1101"/>
        <location filename="../oscar/oximeterimport.ui" line="1353"/>
        <source>Duration</source>
        <translation>Duração</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1217"/>
        <source>Pulse Rate</source>
        <translation>Taxa de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1309"/>
        <source>Multiple Sessions Detected</source>
        <translation>Múltiplas Sessões Detectadas</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1348"/>
        <source>Start Time</source>
        <translation>Tempo de Início</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1358"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1375"/>
        <source>Import Completed. When did the recording start?</source>
        <translation>Importação Completada. Quando a gravação começou?</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1445"/>
        <source>Oximeter Starting time</source>
        <translation>Tempo de Início do Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1457"/>
        <source>I want to use the time reported by my oximeter&apos;s built in clock.</source>
        <translation>Eu gostaria de usar o tempo relatado pelo relógio interno do meu oxímetro.</translation>
    </message>
    <message>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP machine.</source>
        <translation type="vanished">Eu comecei essa gravação de oxímetro ao mesmo (ou próximo) tempo que minha máquina CPAP.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1534"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: Syncing to CPAP session starting time will always be more accurate.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nota: Sincronizar para o tempo de início do CPAP será mais preciso sempre.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1555"/>
        <source>Choose CPAP session to sync to:</source>
        <translation>Escolha uma sessão CPAP com a qual sincronizar:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1675"/>
        <source>You can manually adjust the time here if required:</source>
        <translation>Você pode ajustar manualmente o tempo aqui se necessário:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1696"/>
        <source>HH:mm:ssap</source>
        <translation>HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1793"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1774"/>
        <source>&amp;Information Page</source>
        <translation>Página de &amp;Informação</translation>
    </message>
    <message>
        <source>CMS50D+/E/F, Pulox PO-200/300</source>
        <translation type="vanished">CMS50D+/E/F, Pulox PO-200/300</translation>
    </message>
    <message>
        <source>ChoiceMMed MD300W1</source>
        <translation type="vanished">ChoiceMMed MD300W1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="688"/>
        <source>Set device date/time</source>
        <translation>Definir data/hora do aparelho</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="695"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check to enable updating the device identifier next import, which is useful for those who have multiple oximeters lying around.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Marque para ativar a atualização do identificador do aparelho na próxima importação, o que é útil para aqueles que possuem múltiplos oxímeros largados por perto.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="698"/>
        <source>Set device identifier</source>
        <translation>Definir identificador do aparelho</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="761"/>
        <source>Erase session after successful upload</source>
        <translation>Apagar sessão após envio concluído</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="793"/>
        <source>Import directly from a recording on a device</source>
        <translation>Importar diretamente de uma gravação num aparelho</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="918"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Reminder for CPAP users: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Did you remember to import your CPAP sessions first?&lt;br/&gt;&lt;/span&gt;If you forget, you won&apos;t have a valid time to sync this oximetry session to.&lt;br/&gt;To a ensure good sync between devices, always try to start both at the same time.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600; font-style:italic;&quot;&gt;Lembrete para utilizadores de CPAP: &lt;/span&gt;&lt;span style=&quot; color:#fb0000;&quot;&gt;Você lembrou de importar as sessões do CPAP primeiro?&lt;br/&gt;&lt;/span&gt;Se você esqueceu, você não terá um tempo válido para o qual sincronizar essa sessão de oximetria.&lt;br/&gt;Para garantir uma boa sincronia entre aparelhos, sempre tente iniciar ambos ao mesmo tempo.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1325"/>
        <source>Please choose which one you want to import into OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1399"/>
        <source>Day recording (normally would have) started</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1473"/>
        <source>I started this oximeter recording at (or near) the same time as a session on my CPAP device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1502"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;OSCAR needs a starting time to know where to save this oximetry session to.&lt;/p&gt;&lt;p&gt;Choose one of the following options:&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1812"/>
        <source>&amp;Retry</source>
        <translation>Tenta&amp;r Novamente</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1831"/>
        <source>&amp;Choose Session</source>
        <translation>Es&amp;colher Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1850"/>
        <source>&amp;End Recording</source>
        <translation>T&amp;erminar Gravação</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1869"/>
        <source>&amp;Sync and Save</source>
        <translation>&amp;Sincronizar e Salvar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1888"/>
        <source>&amp;Save and Finish</source>
        <translation>&amp;Salvar e Terminar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.ui" line="1907"/>
        <source>&amp;Start</source>
        <translation>Ini&amp;ciar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="189"/>
        <source>Scanning for compatible oximeters</source>
        <translation>Buscando por oxímetros compatíveis</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="221"/>
        <source>Could not detect any connected oximeter devices.</source>
        <translation>Nenhum oxímetro conectado foi detectado.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="229"/>
        <source>Connecting to %1 Oximeter</source>
        <translation>Conectando ao Oxímetro %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="257"/>
        <source>Renaming this oximeter from &apos;%1&apos; to &apos;%2&apos;</source>
        <translation>Renomeando esse oxímetro de &apos;%1&apos; para &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="260"/>
        <source>Oximeter name is different.. If you only have one and are sharing it between profiles, set the name to the same on both profiles.</source>
        <translation>O nome do oxímetro é diferente.. Se você possui apenas um e está compartilhando-o entre perfis, defina o mesmo nome em todos os perfis.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="303"/>
        <source>&quot;%1&quot;, session %2</source>
        <translation>&quot;%1&quot;, sessão %2</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="328"/>
        <source>Nothing to import</source>
        <translation>Nada para importar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="329"/>
        <source>Your oximeter did not have any valid sessions.</source>
        <translation>Seu oxímetro não tinha quaisquer sessões válidas.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="330"/>
        <source>Close</source>
        <translation>Fechar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="333"/>
        <source>Waiting for %1 to start</source>
        <translation>Aguardando %1 para começar</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="334"/>
        <source>Waiting for the device to start the upload process...</source>
        <translation>Aguardando pelo dispositivo para iniciar o processo de envio...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="336"/>
        <source>Select upload option on %1</source>
        <translation>Selecione a opção upload em %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="337"/>
        <source>You need to tell your oximeter to begin sending data to the computer.</source>
        <translation>Você precisa dizer ao oxímetro para começar a enviar dados para o computador.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="338"/>
        <source>Please connect your oximeter, enter it&apos;s menu and select upload to commence data transfer...</source>
        <translation>Por favor conecte seu oxímetro, entre no menu e selecione upload para começar a transferência de dados...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="368"/>
        <source>%1 device is uploading data...</source>
        <translation>Dispositivo %1 está enviando dados...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="369"/>
        <source>Please wait until oximeter upload process completes. Do not unplug your oximeter.</source>
        <translation>Por favor aguarde até o processo de envio de dados do oxímetro terminar. Não desconecte seu oxímetro.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="388"/>
        <source>Oximeter import completed..</source>
        <translation>Importação do oxímetro completada..</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="413"/>
        <source>Select a valid oximetry data file</source>
        <translation>Selecione um arquivo de dados válido de oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="413"/>
        <source>Oximetry Files (*.spo *.spor *.spo2 *.SpO2 *.dat)</source>
        <translation>Arquivos de Oximetria (*.so *.spor *.spo2 *.SpO2 *.dat)</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="435"/>
        <source>No Oximetry module could parse the given file:</source>
        <translation>Nenhum módulo de oximetria pode interpretar o arquivo fornecido:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="483"/>
        <source>Live Oximetry Mode</source>
        <translation>Módo de Oximetria em Tempo Real</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="535"/>
        <source>Live Oximetry Stopped</source>
        <translation>Oximetria em Tempo Real Parada</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="536"/>
        <source>Live Oximetry import has been stopped</source>
        <translation>A importação de oximetria em tempo real foi parada</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1096"/>
        <source>Oximeter Session %1</source>
        <translation>Sessão de Oxímetro %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1141"/>
        <source>OSCAR gives you the ability to track Oximetry data alongside CPAP session data, which can give valuable insight into the effectiveness of CPAP treatment. It will also work standalone with your Pulse Oximeter, allowing you to store, track and review your recorded data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1143"/>
        <source>OSCAR is currently compatible with Contec CMS50D+, CMS50E, CMS50F and CMS50I serial oximeters.&lt;br/&gt;(Note: Direct importing from bluetooth models is &lt;span style=&quot; font-weight:600;&quot;&gt;probably not&lt;/span&gt; possible yet)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1152"/>
        <source>If you are trying to sync oximetry and CPAP data, please make sure you imported your CPAP sessions first before proceeding!</source>
        <translation>Se você está tentando sincronizar dados de oximetria e CPAP, por favor certifique de que você importou sua sessão de CPAP antes de prosseguir!</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1155"/>
        <source>For OSCAR to be able to locate and read directly from your Oximeter device, you need to ensure the correct device drivers (eg. USB to Serial UART) have been installed on your computer. For more information about this, %1click here%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="465"/>
        <source>Oximeter not detected</source>
        <translation>Oxímetro não detectado</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="472"/>
        <source>Couldn&apos;t access oximeter</source>
        <translation>Impossível acessar oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="486"/>
        <source>Starting up...</source>
        <translation>Inicializando...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="487"/>
        <source>If you can still read this after a few seconds, cancel and try again</source>
        <translation>Se você ainda pode ler isso após alguns segundos, cancele e tente novamente</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="534"/>
        <source>Live Import Stopped</source>
        <translation>Importação em Tempo Real Parada</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="587"/>
        <source>%1 session(s) on %2, starting at %3</source>
        <translation>%1 sessão(ões( em %2, começando em %3</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="591"/>
        <source>No CPAP data available on %1</source>
        <translation>Nenhum dado de CPAP disponível em %1</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="726"/>
        <source>Recording...</source>
        <translation>Gravando...</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="733"/>
        <source>Finger not detected</source>
        <translation>Dedo não detectado</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="833"/>
        <source>I want to use the time my computer recorded for this live oximetry session.</source>
        <translation>Eu quero usar a hora que meu computador registrou para essa sessão de oximetria em tempo real.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="836"/>
        <source>I need to set the time manually, because my oximeter doesn&apos;t have an internal clock.</source>
        <translation>Eu preciso definir o tempo manualmente, porque meu oxímetro não possui relógio interno.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="848"/>
        <source>Something went wrong getting session data</source>
        <translation>Algo deu errado ao obter os dados da sessão</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1137"/>
        <source>Welcome to the Oximeter Import Wizard</source>
        <translation>Bem-vindo ao Assistente de Importação de Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1139"/>
        <source>Pulse Oximeters are medical devices used to measure blood oxygen saturation. During extended Apnea events and abnormal breathing patterns, blood oxygen saturation levels can drop significantly, and can indicate issues that need medical attention.</source>
        <translation>Oxímetros de pulso são dispositivos médicos usados para medir a saturação de oxigênio no sangue. Durante eventos extensos de apenai e padrões anormais de respiração, os níveis de saturação de oxigênio no sangue podem cair significativamente, e podem indicar problemas que precisam de atenção médica.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1145"/>
        <source>You may wish to note, other companies, such as Pulox, simply rebadge Contec CMS50&apos;s under new names, such as the Pulox PO-200, PO-300, PO-400. These should also work.</source>
        <translation>Você pode desejar notar, outras companhias, como a Pulox, simplesmente rotulam o Contec CMS50 sob nomes novos, como o Pulox PO-200, PO-300, PO-400. Estes devem funcionar também.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1148"/>
        <source>It also can read from ChoiceMMed MD300W1 oximeter .dat files.</source>
        <translation>Ele também pode ler dos arquivos .dat do ChoiceMMed MD300W1.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1150"/>
        <source>Please remember:</source>
        <translation>Por favor lembre:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1154"/>
        <source>Important Notes:</source>
        <translation>Notas importantes:</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1157"/>
        <source>Contec CMS50D+ devices do not have an internal clock, and do not record a starting time. If you do not have a CPAP session to link a recording to, you will have to enter the start time manually after the import process is completed.</source>
        <translation>Aparelhos CMS50D+ não possuem relógio interno e não registram um tempo de inínio, se você não possui uma sessão de CPAP para sincronizar a gravação, você terá de especificar manualmente o tempo de início após completar o processo de importação.</translation>
    </message>
    <message>
        <location filename="../oscar/oximeterimport.cpp" line="1159"/>
        <source>Even for devices with an internal clock, it is still recommended to get into the habit of starting oximeter records at the same time as CPAP sessions, because CPAP internal clocks tend to drift over time, and not all can be reset easily.</source>
        <translation>Mesmo para aparelhos com um relógio interno, ainda é recomendado desenvolver o hábito de iniciar as gravações de oxímetro ao mesmo tempo que as sessões de CPAP, porque os relógios internos dos CPAPs tendem a desviar com o tempo, e nem todos podem ser redefinidos facilmente.</translation>
    </message>
</context>
<context>
    <name>Oximetry</name>
    <message>
        <location filename="../oscar/oximetry.ui" line="89"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="102"/>
        <source>d/MM/yy h:mm:ss AP</source>
        <translation>d/MM/yy HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="131"/>
        <source>R&amp;eset</source>
        <translation>R&amp;edefinir</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="245"/>
        <source>Pulse</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="366"/>
        <source>&amp;Open .spo/R File</source>
        <translation>Abrir arquivo .sp&amp;o/R</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="385"/>
        <source>Serial &amp;Import</source>
        <translation>&amp;Importação Serial</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="398"/>
        <source>&amp;Start Live</source>
        <translation>Ini&amp;ciar em Tempo Real</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="421"/>
        <source>Serial Port</source>
        <translation>Porta Serial</translation>
    </message>
    <message>
        <location filename="../oscar/oximetry.ui" line="450"/>
        <source>&amp;Rescan Ports</source>
        <translation>&amp;Rebuscar Portas</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="29"/>
        <source>Preferences</source>
        <translation>Preferências</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="70"/>
        <source>&amp;Import</source>
        <translation>&amp;Importar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="160"/>
        <source>Combine Close Sessions </source>
        <translation>Combinar Sessões Fechadas </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="170"/>
        <location filename="../oscar/preferencesdialog.ui" line="255"/>
        <location filename="../oscar/preferencesdialog.ui" line="753"/>
        <source>Minutes</source>
        <translation>Minutos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="190"/>
        <source>Multiple sessions closer together than this value will be kept on the same day.
</source>
        <translation>Múltiplas sessões mais próximas do que esse valor serão mantidas no mesmo dia.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="245"/>
        <source>Ignore Short Sessions</source>
        <translation>Ignorar Sessões Curtas</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Sessões mais curtas do que isso em duração não serão mostradas&lt;span style=&quot; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-style:italic;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="311"/>
        <source>Day Split Time</source>
        <translation>Tempo de Divisão do Dia</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="321"/>
        <source>Sessions starting before this time will go to the previous calendar day.</source>
        <translation>Sessões iniciando antes dessa hora irão para o dia anterior no calendário.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="394"/>
        <source>Session Storage Options</source>
        <translation>Opções de Armazenamento de Sessões</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="484"/>
        <source>Create SD Card Backups during Import (Turn this off at your own peril!)</source>
        <translation>Criar backups do cartão SD durante a importação (desative isso por sua conta e risco!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="441"/>
        <source>Compress SD Card Backups (slower first import, but makes backups smaller)</source>
        <translation>Comprimir os Backups do Cartão SD (primeira importação mais lenta, mas os backups ficam mais leves)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="659"/>
        <source>&amp;CPAP</source>
        <translation>&amp;CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1297"/>
        <source>Regard days with under this usage as &quot;incompliant&quot;. 4 hours is usually considered compliant.</source>
        <translation>Considere dias com uso abaixo disso &quot;inobservante&quot; 4 horas é geralmente considerado observante.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1300"/>
        <source> hours</source>
        <translation> horas</translation>
    </message>
    <message>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the machine missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation type="vanished">Ativar/desativar melhorias experimentais na marcação de eventos.
Elas permitem detectar eventos limítrofes, e alguns que a máquina deixa passar.
Essa opção deve ser ativada antes da importação, do contrário uma limpeza é necessária.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1058"/>
        <source>Flow Restriction</source>
        <translation>Restrição de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1095"/>
        <source>Percentage of restriction in airflow from the median value. 
A value of 20% works well for detecting apneas. </source>
        <translation>Porcentagem da restrição em fluxo de ar do valor médio.
Um valor de 20% funciona bem para detectar apneias. </translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the machine. They are &lt;span style=&quot; text-decoration: underline;&quot;&gt;not&lt;/span&gt; included in AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:italic;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;As marcações personalizadas são um método experimental de detecção de eventos que a máquina deixa passar. Eles &lt;span style=&quot; text-decoration: underline;&quot;&gt;não&lt;/span&gt; são incluídos no AHI.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1116"/>
        <source>Duration of airflow restriction</source>
        <translation>Duração da restrição de fluxo de ar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="999"/>
        <location filename="../oscar/preferencesdialog.ui" line="1119"/>
        <location filename="../oscar/preferencesdialog.ui" line="1592"/>
        <location filename="../oscar/preferencesdialog.ui" line="1680"/>
        <location filename="../oscar/preferencesdialog.ui" line="1709"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1155"/>
        <source>Event Duration</source>
        <translation>Duração de Evento</translation>
    </message>
    <message>
        <source>Allow duplicates near machine events.</source>
        <translation type="vanished">Permitir duplicados próximos de eventos da máquina.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1220"/>
        <source>Adjusts the amount of data considered for each point in the AHI/Hour graph.
Defaults to 60 minutes.. Highly recommend it&apos;s left at this value.</source>
        <translation>Ajuste a quantidade de dados considerada para cada ponto no gráfico AHI/Hora.
Padrão em 60 minutos.. Altamente recomendado manter nesse valor.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1224"/>
        <source> minutes</source>
        <translation> minutos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1262"/>
        <source>Reset the counter to zero at beginning of each (time) window.</source>
        <translation>Redefinir o contador a zero no começo de cada janela (de tempo).</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1265"/>
        <source>Zero Reset</source>
        <translation>Redefinir a Zero</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="706"/>
        <source>CPAP Clock Drift</source>
        <translation>Deslocamento do Relógio do CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="502"/>
        <source>Do not import sessions older than:</source>
        <translation>Não importar sessões mais antigas que:</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="509"/>
        <source>Sessions older than this date will not be imported</source>
        <translation>Sessões mais antigas que essa data não serão importadas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="535"/>
        <source>dd MMMM yyyy</source>
        <translation>dd MMMM yyyy</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1109"/>
        <source>Show in Event Breakdown Piechart</source>
        <translation>Mostrar no Gráfico Torta de Discriminação de Evento</translation>
    </message>
    <message>
        <source>Resync Machine Detected Events (Experimental)</source>
        <translation type="vanished">Resincronizar Eventos Detectados pela Máquina (Experimental)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1278"/>
        <source>User definable threshold considered large leak</source>
        <translation>Limitar definível pelo utilizador considerando vazamento grande</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1245"/>
        <source>Whether to show the leak redline in the leak graph</source>
        <translation>Mostrar ou não a linha vermelha de vazamento no gráfico de vazamentos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1816"/>
        <location filename="../oscar/preferencesdialog.ui" line="1895"/>
        <source>Search</source>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1518"/>
        <source>&amp;Oximetry</source>
        <translation>&amp;Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1732"/>
        <source>Percentage drop in oxygen saturation</source>
        <translation>Queda de porcentagem na saturação de oxigênio</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1725"/>
        <source>Pulse</source>
        <translation>Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1690"/>
        <source>Sudden change in Pulse Rate of at least this amount</source>
        <translation>Mudança brusca na Taxa de Pulso de pelo menos essa intensidade</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1582"/>
        <location filename="../oscar/preferencesdialog.ui" line="1612"/>
        <location filename="../oscar/preferencesdialog.ui" line="1693"/>
        <source> bpm</source>
        <translation> bpm</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1677"/>
        <source>Minimum duration of drop in oxygen saturation</source>
        <translation>Duração mínima da queda na saturação de oxigênio</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1706"/>
        <source>Minimum duration of pulse change event.</source>
        <translation>Duração mínima do evento de mudança no pulso.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1589"/>
        <source>Small chunks of oximetry data under this amount will be discarded.</source>
        <translation>Pequenos pedaços dos dados de oximetria abaixo desse valor serão descartados.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1951"/>
        <source>&amp;General</source>
        <translation>&amp;Geral</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1965"/>
        <source>General Settings</source>
        <translation>Configurações Gerais</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2706"/>
        <source>Daily view navigation buttons will skip over days without data records</source>
        <translation>Botões de navegação na visão diária irão pular dias sem dados registrados</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2709"/>
        <source>Skip over Empty Days</source>
        <translation>Pular sobre Dias Vazios</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1986"/>
        <source>Allow use of multiple CPU cores where available to improve performance. 
Mainly affects the importer.</source>
        <translation>Permitir múltiplos núcleos de CPU quando disponíveis para melhorar desempenho.
Afeta principalmente a importação.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1990"/>
        <source>Enable Multithreading</source>
        <translation>Ativar Multithreading</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="575"/>
        <source>Bypass the login screen and load the most recent User Profile</source>
        <translation>Ignorar a tela de login e carregar o perfil de utilizador mais recente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2696"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;These features have recently been pruned. They will come back later. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Esses recursos foram retirados recentemente. Eles retornarão no futuro. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1345"/>
        <source>Changes to the following settings needs a restart, but not a recalc.</source>
        <translation>Mudanças nas configurações seguintes exige um reinício, mas não uma recalculação.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1348"/>
        <source>Preferred Calculation Methods</source>
        <translation>Métodos de Cálculo Preferidos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1377"/>
        <source>Middle Calculations</source>
        <translation>Calculações de Meio</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1391"/>
        <source>Upper Percentile</source>
        <translation>Percentil Superior</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1354"/>
        <source>For consistancy, ResMed users should use 95% here,
as this is the only value available on summary-only days.</source>
        <translation>Para consistência, utilizadores ResMed devem usar 95% aqui,
já que esse é o único valor disponível nos dias de apenas resumo.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1405"/>
        <source>Median is recommended for ResMed users.</source>
        <translation>Mediana é recomendada para utilizadores ResMed.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1409"/>
        <location filename="../oscar/preferencesdialog.ui" line="1472"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1414"/>
        <source>Weighted Average</source>
        <translation>Média Ponderada</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1419"/>
        <source>Normal Average</source>
        <translation>Média Normal</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1443"/>
        <source>True Maximum</source>
        <translation>Máximo Verdadeiro</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1448"/>
        <source>99% Percentile</source>
        <translation>Percil de 99%</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1384"/>
        <source>Maximum Calcs</source>
        <translation>Cálcs Máximos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="97"/>
        <source>Session Splitting Settings</source>
        <translation>Configurações de Separação de Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="360"/>
        <source>Don&apos;t Split Summary Days (Warning: read the tooltip!)</source>
        <translation>Não Separar Dias Resumidos (Aviso: leia a dica de contexto!)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="569"/>
        <source>Memory and Startup Options</source>
        <translation>Opções de Memória e Inicialização</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="611"/>
        <source>Pre-Load all summary data at startup</source>
        <translation>Pré-carregar todos os dados resumidos no início</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="598"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This setting keeps waveform and event data in memory after use to speed up revisiting days.&lt;/p&gt;&lt;p&gt;This is not really a necessary option, as your operating system caches previously used files too.&lt;/p&gt;&lt;p&gt;Recommendation is to leave it switched off, unless your computer has a ton of memory.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Essa opção mantem os dados de forma de onda e eventos na memória após o uso para acelerar a revisitação de dias.&lt;/p&gt;&lt;p&gt;Essa não é uma opção realmente necessária, já que seu sistema operativo retém arquivos previamente utilizados.&lt;/p&gt;&lt;p&gt;A recomendação é deixá-la desativada, a menos que seu computador possua muita memória livre.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="601"/>
        <source>Keep Waveform/Event data in memory</source>
        <translation>Manter dados de forma de onda/eventos na memória</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="625"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cuts down on any unimportant confirmation dialogs during import.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Reduzir quaisquer diálogos de confirmação sem importância durante a importação.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="628"/>
        <source>Import without asking for confirmation</source>
        <translation>Importar sem pedir confirmação</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1184"/>
        <source>General CPAP and Related Settings</source>
        <translation>Configurações Gerais e Relacionadas ao CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1193"/>
        <source>Enable Unknown Events Channels</source>
        <translation>Ativar Canais de Evento Desconhecidos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1323"/>
        <source>AHI</source>
        <extracomment>Apnea Hypopnea Index</extracomment>
        <translatorcomment>Índice de Apneia-Hipoapneia</translatorcomment>
        <translation>IAH</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1328"/>
        <source>RDI</source>
        <extracomment>Respiratory Disturbance Index</extracomment>
        <translatorcomment>Índice de Distúrbio Respiratório</translatorcomment>
        <translation>IDR</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1200"/>
        <source>AHI/Hour Graph Time Window</source>
        <translation>Janela de Tempo do Gráfico IAH/Hora</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1255"/>
        <source>Preferred major event index</source>
        <translation>Índice preferido de evento principal</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1207"/>
        <source>Compliance defined as</source>
        <translation>Observância definida como</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1248"/>
        <source>Flag leaks over threshold</source>
        <translation>Marcar vazamentos acima do limite</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="787"/>
        <source>Seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="733"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Note: This is not intended for timezone corrections! Make sure your operating system clock and timezone is set correctly.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Nota: Isso não é pretendido para correções de fuso-horário! Certifique-se de que o fuso-horário e o relógio do seu sistema estão configurados corretamente.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="780"/>
        <source>Hours</source>
        <translation>Horas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1439"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;True maximum is the maximum of the data set.&lt;/p&gt;&lt;p&gt;99th percentile filters out the rarest outliers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Máximo verdadeiro é o máximo do conjunto de dados.&lt;/p&gt;&lt;p&gt;99º percentil filtra os mais raros distanciamentos.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1457"/>
        <source>Combined Count divided by Total Hours</source>
        <translation>Contagem combinada divididade pelas horas totais</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1462"/>
        <source>Time Weighted average of Indice</source>
        <translation>A Média Ponderada pelo tempo do Índice</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1467"/>
        <source>Standard average of indice</source>
        <translation>Média padrão do índice</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="433"/>
        <source>Compress ResMed (EDF) backups to save disk space.
Backed up EDF files are stored in the .gz format, 
which is common on Mac &amp; Linux platforms.. 

OSCAR can import from this compressed backup directory natively.. 
To use it with ResScan will require the .gz files to be uncompressed first..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="272"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt;&quot;&gt;Sessions shorter in duration than this will not be displayed&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Cantarell&apos;; font-size:11pt; font-style:italic;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="453"/>
        <source>The following options affect the amount of disk space OSCAR uses, and have an effect on how long import takes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="463"/>
        <source>This makes OSCAR&apos;s data take around half as much space.
But it makes import and day changing take longer.. 
If you&apos;ve got a new computer with a small solid state disk, this is a good option.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="468"/>
        <source>Compress Session Data (makes OSCAR data smaller, but day changing slower.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="608"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes starting OSCAR a bit slower, by pre-loading all the summary data in advance, which speeds up overview browsing and a few other calculations later on. &lt;/p&gt;&lt;p&gt;If you have a large amount of data, it might be worth keeping this switched off, but if you typically like to view &lt;span style=&quot; font-style:italic;&quot;&gt;everything&lt;/span&gt; in overview, all the summary data still has to be loaded anyway. &lt;/p&gt;&lt;p&gt;Note this setting doesn&apos;t affect waveform and event data, which is always demand loaded as needed.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="972"/>
        <source>Custom CPAP User Event Flagging</source>
        <translation>Marcação de Evento Personalizada pelo Utilizador de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1076"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Custom flagging is an experimental method of detecting events missed by the device. They are &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; included in AHI.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1997"/>
        <source>Show Remove Card reminder notification on OSCAR shutdown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2105"/>
        <source>Check for new version every</source>
        <translation>Buscar uma nova versão a cada</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2128"/>
        <source>days.</source>
        <translation>dias.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2158"/>
        <source>Last Checked For Updates: </source>
        <translation>Última Busca Por Atualizações: </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2171"/>
        <source>TextLabel</source>
        <translation>Rótulo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2196"/>
        <source>I want to be notified of test versions. (Advanced users only please.)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2239"/>
        <source>&amp;Appearance</source>
        <translation>&amp;Aparência</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2268"/>
        <source>Graph Settings</source>
        <translation>Configurações de Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2284"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Which tab to open on loading a profile. (Note: It will default to Profile if OSCAR is set to not open a profile on startup)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2542"/>
        <source>Bar Tops</source>
        <translation>Topo de Barra</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2547"/>
        <source>Line Chart</source>
        <translation>Gráficos de Linha</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2637"/>
        <source>Overview Linecharts</source>
        <translation>Visão-Geral de Gráficos de Linha</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2582"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This makes scrolling when zoomed in easier on sensitive bidirectional TouchPads&lt;/p&gt;&lt;p&gt;50ms is recommended value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Isso facilita a rolagem quando o zoom é mais fácil em TouchPads bidirecionais sensíveis.&lt;/p&gt;&lt;p&gt;50 ms é o valor recomendado.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2464"/>
        <source>Scroll Dampening</source>
        <translation>Amortecimento de rolagem</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2384"/>
        <source>Overlay Flags</source>
        <translation>Marcações Sobrepostas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2394"/>
        <source>Line Thickness</source>
        <translation>Espessura de Linha</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2430"/>
        <source>The pixel thickness of line plots</source>
        <translation>Espessura em pixels dos traços de linha</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2686"/>
        <source>Pixmap caching is an graphics acceleration technique. May cause problems with font drawing in graph display area on your platform.</source>
        <translation>Cache de pixmap é uma técnica de aceleração gráfica. Pode causar problemas no desenho de fontes na área de mostragem de gráficos na sua plataforma.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2036"/>
        <source>Try changing this from the default setting (Desktop OpenGL) if you experience rendering problems with OSCAR&apos;s graphs.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2771"/>
        <source>Fonts (Application wide settings)</source>
        <translation>Fontes (afeta o aplicativo todo)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2410"/>
        <source>The visual method of displaying waveform overlay flags.
</source>
        <translation>O método visual de mostrar marcações de sobreposição de formas de onda.
</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2415"/>
        <source>Standard Bars</source>
        <translation>Barras Padrão</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2368"/>
        <source>Graph Height</source>
        <translation>Altura do Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2561"/>
        <source>Default display height of graphs in pixels</source>
        <translation>Altura de exibição, em pixels, padrão dos gráficos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2476"/>
        <source>How long you want the tooltips to stay visible.</source>
        <translation>Por quanto tempo você deseja que as dicas de contexto permaneçam visíveis.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1793"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1846"/>
        <location filename="../oscar/preferencesdialog.ui" line="1925"/>
        <source>Reset &amp;Defaults</source>
        <translation>Redefinir Pa&amp;drões</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1859"/>
        <location filename="../oscar/preferencesdialog.ui" line="1938"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;Just because you can, does not mean it&apos;s good practice.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Aviso: &lt;/span&gt;Só porque você pode, não significa que é uma boa prática.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1872"/>
        <source>Waveforms</source>
        <translation>Formas de Onda</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1658"/>
        <source>Flag rapid changes in oximetry stats</source>
        <translation>Marcar mudanças bruscas nos números de oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1569"/>
        <source>Other oximetry options</source>
        <translation>Outras opções de oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1602"/>
        <source>Discard segments under</source>
        <translation>Descartar segmentos abaixo de</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1639"/>
        <source>Flag Pulse Rate Above</source>
        <translation>Marcar Taxa de Pulso Acima de</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1629"/>
        <source>Flag Pulse Rate Below</source>
        <translation>Marcar Taxa de Pulso Abaixo de</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="357"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;This setting should be used with caution...&lt;/span&gt; Switching it off comes with consequences involving accuracy of summary only days, as certain calculations only work properly provided summary only sessions that came from individual day records are kept together. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;ResMed users:&lt;/span&gt; Just because it seems natural to you and I that the 12 noon session restart should be in the previous day, does not mean ResMed&apos;s data agrees with us. The STF.edf summary index format has serious weaknesses that make doing this not a good idea.&lt;/p&gt;&lt;p&gt;This option exists to pacify those who don&apos;t care and want to see this &amp;quot;fixed&amp;quot; no matter the costs, but know it comes with a cost. If you keep your SD card in every night, and import at least once a week, you won&apos;t see problems with this very often.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Essa configuração deve ser usada com cautela...&lt;/span&gt; Desativá-la traz consequências envolvendo a precisão de dias resumo-apenas, pois certos cálculos só funcionam corretamente se sessões de resumo provenientes de registros de dias individuais forem mantidas juntas. &lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Utilizadores ResMed:&lt;/span&gt; Apenas porque parece natural para você e eu que a sessão reiniciada ao meio-dia deve estar no dia anterior, não significa que os dados da ResMed concordam conosco. O formato de índice de resumo STF.edf tem sérios pontos fracos que fazem com que isso não seja uma boa idéia.&lt;/p&gt;&lt;p&gt;Esta opção existe para apaziguar aqueles que não se importam e querem ver isso &amp;quot;corrigido&amp;quot; não importando os custos, mas saiba que isso tem um custo. Se você mantiver seu cartão SD inserido todas as noites, e importar pelo menos uma vez por semana, você não verá problemas com isso com grande frequência..&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>This calculation requires Total Leaks data to be provided by the CPAP machine. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation type="vanished">Este cálculo requer dados de vazamentos totais a serem fornecidos pela máquina de CPAP. (Por exemplo, PRS1, mas não a ResMed, que já tem isso)

Os cálculos de vazamentos não intencionais usados aqui são lineares, eles não modelam a curva de ventilação da máscara.

Se você usar algumas máscaras diferentes, escolha valores médios. E deve ficar próximo o suficiente.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="810"/>
        <source>Calculate Unintentional Leaks When Not Present</source>
        <translation>Calcular Vazamentos Não Intencionais Quando Não Presente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="906"/>
        <source>4 cmH2O</source>
        <translation>4 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="916"/>
        <source>20 cmH2O</source>
        <translation>20 cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="948"/>
        <source>Note: A linear calculation method is used. Changing these values requires a recalculation.</source>
        <translation>Nota: Um método de cálculo linear é usado. Alterar esses valores requer um recálculo.</translation>
    </message>
    <message>
        <source>Show flags for machine detected events that haven&apos;t been identified yet.</source>
        <translation type="vanished">Mostrar marcações para eventos detectados por máquina que ainda não foram identificados.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2454"/>
        <source>Tooltip Timeout</source>
        <translation>Tempo Limite de Dica de Contexto</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2534"/>
        <source>Graph Tooltips</source>
        <translation>Dicas de Contexto de Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2420"/>
        <source>Top Markers</source>
        <translation>Marcadores de Topo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="423"/>
        <source>Changing SD Backup compression options doesn&apos;t automatically recompress backup data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="578"/>
        <source>Auto-Launch CPAP Importer after opening profile</source>
        <translation>Iniciar o importador de CPAP automaticamente depois de abrir o perfil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="618"/>
        <source>Automatically load last used profile on start-up</source>
        <translation>Carregar automaticamente o perfil usado por último ao abrir</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="645"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data that is somehow different from anything previously seen by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="648"/>
        <source>Warn when previously unseen data is encountered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="819"/>
        <source>Your masks vent rate at 20 cmH2O pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="875"/>
        <source>Your masks vent rate at 4 cmH2O pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1281"/>
        <source> l/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1398"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Cumulative Indices&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed machines do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Devido a limitações no projeto dos resumos, as máquinas da ResMed não suportam a alteração dessas opções.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1539"/>
        <source>Oximetry Settings</source>
        <translation>Opções de oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1619"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Flag SpO&lt;span style=&quot; vertical-align:sub;&quot;&gt;2&lt;/span&gt; Desaturations Below&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1779"/>
        <source> &lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt; p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Segoe UI&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600;&quot;&gt;Syncing Oximetry and CPAP Data&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;CMS50 data imported from SpO2Review (from .spoR files) or the serial import method do &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:600; text-decoration: underline;&quot;&gt;not&lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt; have the correct timestamp needed to sync.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;Live view mode (using a serial cable) is one way to acheive an accurate sync on CMS50 oximeters, but does not counter for CPAP clock drift.&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;If you start your Oximeters recording mode at &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-style:italic;&quot;&gt;exactly &lt;/span&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;the same time you start your CPAP device, you can now also achieve sync. &lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;justify&quot; style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans&apos;; font-size:10pt;&quot;&gt;The serial import process takes the starting time from last nights first CPAP session. (Remember to import your CPAP data first!)&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2004"/>
        <source>Always save screenshots in the OSCAR Data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2058"/>
        <source>Check For Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2073"/>
        <source>You are using a test version of OSCAR. Test versions check for updates automatically at least once every seven days.  You may set the interval to less than seven days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2090"/>
        <source>Automatically check for updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2112"/>
        <source>How often OSCAR should check for updates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2193"/>
        <source>If you are interested in helping test new features and bugfixes early, click here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2209"/>
        <source>If you would like to help test early versions of OSCAR, please see the Wiki page about testing OSCAR.  We welcome everyone who would like to test OSCAR, help develop OSCAR, and help with translations to existing or new languages. https://www.sleepfiles.com/OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2274"/>
        <source>On Opening</source>
        <translation>Ao Abrir</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2287"/>
        <location filename="../oscar/preferencesdialog.ui" line="2291"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2296"/>
        <location filename="../oscar/preferencesdialog.ui" line="2335"/>
        <source>Welcome</source>
        <translation>Bem-vindo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2301"/>
        <location filename="../oscar/preferencesdialog.ui" line="2340"/>
        <source>Daily</source>
        <translation>Diariamente</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2311"/>
        <location filename="../oscar/preferencesdialog.ui" line="2350"/>
        <source>Statistics</source>
        <translation>Estatísticas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2319"/>
        <source>Switch Tabs</source>
        <translation>Alternar Abas</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2330"/>
        <source>No change</source>
        <translation>Nenhuma mudança</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2358"/>
        <source>After Import</source>
        <translation>Após Importação</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2656"/>
        <source>Other Visual Settings</source>
        <translation>Outras Opções Visuais</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2662"/>
        <source>Anti-Aliasing applies smoothing to graph plots.. 
Certain plots look more attractive with this on. 
This also affects printed reports.

Try it and see if you like it.</source>
        <translation>O anti-aliasing aplica suavização aos desenhos do gráfico.
Certas parcelas parecem mais atraentes com isso.
Isso também afeta os relatórios impressos.

Experimente e veja se você gosta.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2669"/>
        <source>Use Anti-Aliasing</source>
        <translation>Usar Anti-Aliasing</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2676"/>
        <source>Makes certain plots look more &quot;square waved&quot;.</source>
        <translation>Faz com que certos gráficos pareçam mais &quot;quadrados ondulados&quot;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2679"/>
        <source>Square Wave Plots</source>
        <translation>Gráficos de Onda Quadrada</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2689"/>
        <source>Use Pixmap Caching</source>
        <translation>Usar Cache de Pixmap</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2699"/>
        <source>Animations &amp;&amp; Fancy Stuff</source>
        <translation>Animações e Coisas Chiques</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2716"/>
        <source>Whether to allow changing yAxis scales by double clicking on yAxis labels</source>
        <translation>Permitir ou não a alteração das escalas do EixoY clicando duas vezes nos rótulos do EixoY</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2719"/>
        <source>Allow YAxis Scaling</source>
        <translation>Permitir Escala do EixoY</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2729"/>
        <source>Include Serial Number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2030"/>
        <source>Graphics Engine (Requires Restart)</source>
        <translation>Motor Gráfico (Exige Reinício)</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="475"/>
        <source>This maintains a backup of SD-card data for ResMed devices, 

ResMed S9 series devices delete high resolution data older than 7 days, 
and graph data older than 30 days..

OSCAR can keep a copy of this data if you ever need to reinstall. 
(Highly recomended, unless your short on disk space or don&apos;t care about the graph data)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="635"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Provide an alert when importing data from any device model that has not yet been tested by OSCAR developers.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="638"/>
        <source>Warn when importing data from an untested device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="803"/>
        <source>This calculation requires Total Leaks data to be provided by the CPAP device. (Eg, PRS1, but not ResMed, which has these already)

The Unintentional Leak calculations used here are linear, they don&apos;t model the mask vent curve.

If you use a few different masks, pick average values instead. It should still be close enough.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="967"/>
        <source>Enable/disable experimental event flagging enhancements. 
It allows detecting borderline events, and some the device missed.
This option must be enabled before import, otherwise a purge is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1019"/>
        <source>This experimental option attempts to use OSCAR&apos;s event flagging system to improve device detected event positioning.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1022"/>
        <source>Resync Device Detected Events (Experimental)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1142"/>
        <source>Allow duplicates near device events.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1190"/>
        <source>Show flags for device detected events that haven&apos;t been identified yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="1486"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note: &lt;/span&gt;Due to summary design limitations, ResMed devices do not support changing these settings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2726"/>
        <source>Whether to include device serial number on device settings changes report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2736"/>
        <source>Print reports in black and white, which can be more legible on non-color printers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2739"/>
        <source>Print reports in black and white (monochrome)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2805"/>
        <source>Font</source>
        <translation>Fonte</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2823"/>
        <source>Size</source>
        <translation>Tamanho</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2841"/>
        <source>Bold  </source>
        <translation>Negrito  </translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2862"/>
        <source>Italic</source>
        <translation>Itálico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2875"/>
        <source>Application</source>
        <translation>Aplicativo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2939"/>
        <source>Graph Text</source>
        <translation>Texto do Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3000"/>
        <source>Graph Titles</source>
        <translation>Títulos do Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3061"/>
        <source>Big  Text</source>
        <translation>Texto  Grande</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3127"/>
        <location filename="../oscar/preferencesdialog.cpp" line="466"/>
        <location filename="../oscar/preferencesdialog.cpp" line="598"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3159"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="3166"/>
        <source>&amp;Ok</source>
        <translation>&amp;Ok</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="461"/>
        <location filename="../oscar/preferencesdialog.cpp" line="592"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="462"/>
        <location filename="../oscar/preferencesdialog.cpp" line="593"/>
        <source>Color</source>
        <translation>Cor</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="464"/>
        <source>Flag Type</source>
        <translation>Tipo de Marcação</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="465"/>
        <location filename="../oscar/preferencesdialog.cpp" line="597"/>
        <source>Label</source>
        <translation>Rótulo</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="482"/>
        <source>CPAP Events</source>
        <translation>Eventos CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="483"/>
        <source>Oximeter Events</source>
        <translation>Eventos Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="484"/>
        <source>Positional Events</source>
        <translation>Eventos Posicionais</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="485"/>
        <source>Sleep Stage Events</source>
        <translation>Eventos de Estágio do Sono</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="486"/>
        <source>Unknown Events</source>
        <translation>Eventos Desconhecidos</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="658"/>
        <source>Double click to change the descriptive name this channel.</source>
        <translation>Clique duas vezes para alterar o nome descritivo desse canal.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="536"/>
        <location filename="../oscar/preferencesdialog.cpp" line="665"/>
        <source>Double click to change the default color for this channel plot/flag/data.</source>
        <translation>Clique duas vezes para alterar a cor padrão para esse canal/desenho/marcação/dado.</translation>
    </message>
    <message>
        <source>%1 %2</source>
        <translation type="vanished">%1 %2</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.ui" line="2306"/>
        <location filename="../oscar/preferencesdialog.ui" line="2345"/>
        <location filename="../oscar/preferencesdialog.cpp" line="463"/>
        <location filename="../oscar/preferencesdialog.cpp" line="594"/>
        <source>Overview</source>
        <translation>Visão-Geral</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="66"/>
        <source>No CPAP devices detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="67"/>
        <source>Will you be using a ResMed brand device?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="74"/>
        <source>&lt;p&gt;&lt;b&gt;Please Note:&lt;/b&gt; OSCAR&apos;s advanced session splitting capabilities are not possible with &lt;b&gt;ResMed&lt;/b&gt; devices due to a limitation in the way their settings and summary data is stored, and therefore they have been disabled for this profile.&lt;/p&gt;&lt;p&gt;On ResMed devices, days will &lt;b&gt;split at noon&lt;/b&gt; like in ResMed&apos;s commercial software.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="528"/>
        <source>Double click to change the descriptive name the &apos;%1&apos; channel.</source>
        <translation>Clique duass vezes para mudar o nome descritivo do canal &apos;%1&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="541"/>
        <source>Whether this flag has a dedicated overview chart.</source>
        <translation>Se esse sinalizador possui um gráfico de visão geral dedicado ou não.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="551"/>
        <source>Here you can change the type of flag shown for this event</source>
        <translation>Aqui você pode alterar o tipo de marcação mostrada para este evento</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="556"/>
        <location filename="../oscar/preferencesdialog.cpp" line="689"/>
        <source>This is the short-form label to indicate this channel on screen.</source>
        <translation>Este é o rótulo de forma curta para indicar este canal na tela.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="562"/>
        <location filename="../oscar/preferencesdialog.cpp" line="695"/>
        <source>This is a description of what this channel does.</source>
        <translation>Esta é uma descrição do que esse canal faz.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="595"/>
        <source>Lower</source>
        <translation>Inferior</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="596"/>
        <source>Upper</source>
        <translation>Superior</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="615"/>
        <source>CPAP Waveforms</source>
        <translation>Formas de Onda de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="616"/>
        <source>Oximeter Waveforms</source>
        <translation>Formas de Onda de Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="617"/>
        <source>Positional Waveforms</source>
        <translation>Formas de Onda Posicionais</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="618"/>
        <source>Sleep Stage Waveforms</source>
        <translation>Formas de Onda de Estágio de Sono</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="674"/>
        <source>Whether a breakdown of this waveform displays in overview.</source>
        <translation>Se uma discriminação dessa forma de onda é exibida na visão geral ou não.</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="679"/>
        <source>Here you can set the &lt;b&gt;lower&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Aqui você pode definir o limite &lt;b&gt;inferior&lt;/b&gt; usado para determinados cálculos na forma de onda %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="684"/>
        <source>Here you can set the &lt;b&gt;upper&lt;/b&gt; threshold used for certain calculations on the %1 waveform</source>
        <translation>Aqui você pode definir o limite &lt;b&gt;superior&lt;/b&gt; usado para determinados cálculos na forma de onda %1</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="794"/>
        <source>Data Processing Required</source>
        <translation>Processamento de Dados Requerido</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="795"/>
        <source>A data re/decompression proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Um procedimento de recuperação de dados / descompactação é necessário para aplicar essas alterações. Essa operação pode levar alguns minutos para ser concluída.

Tem certeza de que deseja fazer essas alterações?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="803"/>
        <source>Data Reindex Required</source>
        <translation>Reordenação de Dados de Índice Requerida</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="804"/>
        <source>A data reindexing proceedure is required to apply these changes. This operation may take a couple of minutes to complete.

Are you sure you want to make these changes?</source>
        <translation>Um procedimento de reindexação de dados é necessário para aplicar essas alterações. Essa operação pode levar alguns minutos para ser concluída.

Tem certeza de que deseja fazer essas alterações?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="810"/>
        <source>Restart Required</source>
        <translation>Reinício Requerido</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="811"/>
        <source>One or more of the changes you have made will require this application to be restarted, in order for these changes to come into effect.

Would you like do this now?</source>
        <translation>Uma ou mais das alterações feitas exigirão que esse aplicativo seja reiniciado para que essas alterações entrem em vigor.

Você gostaria de fazer isso agora?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1165"/>
        <source>ResMed S9 devices routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1166"/>
        <source> If you ever need to reimport this data again (whether in OSCAR or ResScan) this data won&apos;t come back.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1167"/>
        <source> If you need to conserve disk space, please remember to carry out manual backups.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1168"/>
        <source> Are you sure you want to disable these backups?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1212"/>
        <source>Switching off backups is not a good idea, because OSCAR needs these to rebuild the database if errors are found.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1213"/>
        <source>Are you really sure you want to do this?</source>
        <translation>Tem certeza mesmo de que deseja fazer isso?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="50"/>
        <source>Flag</source>
        <translation>Marcação</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="51"/>
        <source>Minor Flag</source>
        <translation>Pequena Marcação</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="52"/>
        <source>Span</source>
        <translation>Período</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="53"/>
        <source>Always Minor</source>
        <translation>Sempre Pequeno</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="294"/>
        <source>Never</source>
        <translation>Nunca</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1164"/>
        <source>This may not be a good idea</source>
        <translation>Isso pode não ser uma boa ideia</translation>
    </message>
    <message>
        <source>ResMed S9 machines routinely delete certain data from your SD card older than 7 and 30 days (depending on resolution).</source>
        <translation type="vanished">As máquinas ResMed S9 apagam rotineiramente certos dados do seu cartão SD com mais de 7 e 30 dias (dependendo da resolução).</translation>
    </message>
</context>
<context>
    <name>ProfileSelector</name>
    <message>
        <location filename="../oscar/profileselector.ui" line="26"/>
        <source>Filter:</source>
        <translation>Filtro:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="36"/>
        <source>Reset filter to see all profiles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="198"/>
        <source>Version</source>
        <translation>Versão</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="215"/>
        <source>&amp;Open Profile</source>
        <translation>Abr&amp;ir Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="226"/>
        <source>&amp;Edit Profile</source>
        <translation>&amp;Editar Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="240"/>
        <source>&amp;New Profile</source>
        <translation>&amp;Novo Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="258"/>
        <source>Profile: None</source>
        <translation>Perfil: Nenhum</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="278"/>
        <source>Please select or create a profile...</source>
        <translation>Por favor selecione ou crie um perfil...</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.ui" line="328"/>
        <source>Destroy Profile</source>
        <translation>Destruir Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="90"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="91"/>
        <source>Ventilator Brand</source>
        <translation>Marca do Ventilador</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="92"/>
        <source>Ventilator Model</source>
        <translation>Modelo do Ventilador</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="93"/>
        <source>Other Data</source>
        <translation>Outros Dados</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="94"/>
        <source>Last Imported</source>
        <translation>Importado por Último</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="95"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="170"/>
        <source>You must create a profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="233"/>
        <location filename="../oscar/profileselector.cpp" line="367"/>
        <source>Enter Password for %1</source>
        <translation>Digite a senha para %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="249"/>
        <location filename="../oscar/profileselector.cpp" line="386"/>
        <source>You entered an incorrect password</source>
        <translation>Você digitou uma senha incorreta</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Forgot your password?</source>
        <translation>Esqueceu sua senha?</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="252"/>
        <source>Ask on the forums how to reset it, it&apos;s actually pretty easy.</source>
        <translation>Pergunte nos fóruns como redefiní-la, na verdade é muito fácil.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="322"/>
        <source>Select a profile first</source>
        <translation>Selecione um perfil primeiro</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="357"/>
        <source>The selected profile does not appear to contain any data and cannot be removed by OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="389"/>
        <source>If you&apos;re trying to delete because you forgot the password, you need to either reset it or delete the profile folder manually.</source>
        <translation>Se você está tentando deletar porque você esqueceu a senha, você precisa ou redefiní-la ou deletar a pasta do perfil manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>You are about to destroy profile &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</source>
        <translation>Você está prestes a destruir o perfil &apos;&lt;b&gt;%1&lt;/b&gt;&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>Think carefully, as this will irretrievably delete the profile along with all &lt;b&gt;backup data&lt;/b&gt; stored under&lt;br/&gt;%2.</source>
        <translation>Pense com cuidado, já que isso irá irreverssívelmente deletar o perfil bem como todos os &lt;b&gt;dados de backup&lt;/b&gt; guardados sob&lt;br/&gt;%2.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="399"/>
        <source>Enter the word &lt;b&gt;DELETE&lt;/b&gt; below (exactly as shown) to confirm.</source>
        <translation>Digite a palavra &lt;b&gt;DELETE&lt;/b&gt; abaixo (exatamente como exibida) parar confirmar.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="417"/>
        <source>DELETE</source>
        <translation>DELETE</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="418"/>
        <source>Sorry</source>
        <translation>Desculpe</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="418"/>
        <source>You need to enter DELETE in capital letters.</source>
        <translation>Você precisa digitar DELETE em letras maiúsculas.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="431"/>
        <source>There was an error deleting the profile directory, you need to manually remove it.</source>
        <translation>Houve um erro deletando a pasta do perfil, você precisa removê-lo manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="435"/>
        <source>Profile &apos;%1&apos; was succesfully deleted</source>
        <translation>Perfil &apos;%1&apos; foi deletado com sucesso</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>KB</source>
        <translation>KB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>MB</source>
        <translation>MB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>GB</source>
        <translation>GB</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>TB</source>
        <translation>RC</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="445"/>
        <source>PB</source>
        <translation>RP</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="465"/>
        <source>Summaries:</source>
        <translation>Resumos:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="466"/>
        <source>Events:</source>
        <translation>Eventos:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="467"/>
        <source>Backups:</source>
        <translation>Backups:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="479"/>
        <location filename="../oscar/profileselector.cpp" line="519"/>
        <source>Hide disk usage information</source>
        <translation>Esconder informação de uso de disco</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="482"/>
        <source>Show disk usage information</source>
        <translation>Mostrar informação de uso de disco</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="500"/>
        <source>Name: %1, %2</source>
        <translation>Nome: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="503"/>
        <source>Phone: %1</source>
        <translation>Telefone: %1</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="506"/>
        <source>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</source>
        <translation>Email: &lt;a href=&apos;mailto:%1&apos;&gt;%1&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="509"/>
        <source>Address:</source>
        <translation>Endereço:</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="512"/>
        <source>No profile information given</source>
        <translation>Nenhuma informação de perfil fornecida</translation>
    </message>
    <message>
        <location filename="../oscar/profileselector.cpp" line="515"/>
        <source>Profile: %1</source>
        <translation>Perfil: %1</translation>
    </message>
</context>
<context>
    <name>ProgressDialog</name>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="57"/>
        <source>Abort</source>
        <translation>Abortar</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="842"/>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1248"/>
        <source>No Data</source>
        <translation>Nenhum Dado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="853"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="180"/>
        <source>On</source>
        <translation>Ligado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="854"/>
        <source>Off</source>
        <translation>Desligado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="668"/>
        <source>ft</source>
        <translation>ft</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="669"/>
        <source>lb</source>
        <translation>lb</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="670"/>
        <source>oz</source>
        <translation>oz</translation>
    </message>
    <message>
        <source>Kg</source>
        <translation type="vanished">Kg</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="672"/>
        <source>cmH2O</source>
        <translation>cmH2O</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="231"/>
        <source>Med.</source>
        <translation>Med.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="249"/>
        <source>Min: %1</source>
        <translation>Min: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="280"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="290"/>
        <source>Min: </source>
        <translation>Min: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="285"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="295"/>
        <source>Max: </source>
        <translation>Max: </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="314"/>
        <source>Max: %1</source>
        <translation>Max: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="320"/>
        <source>%1 (%2 days): </source>
        <translation>%1 (%2 dias): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="322"/>
        <source>%1 (%2 day): </source>
        <translation>%1 (%2 dia): </translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="381"/>
        <source>% in %1</source>
        <translation>% em %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="387"/>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="719"/>
        <location filename="../oscar/SleepLib/common.cpp" line="673"/>
        <source>Hours</source>
        <translation>Horas</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="393"/>
        <source>Min %1</source>
        <translation>Mín %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="704"/>
        <source>
Hours: %1</source>
        <translation>
Horas: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="770"/>
        <source>%1 low usage, %2 no usage, out of %3 days (%4% compliant.) Length: %5 / %6 / %7</source>
        <translation>%1 baixo uso, %2 nenhum uso, de %3 dias (%4% obervância). Duração: %5 / %6 / %7</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="851"/>
        <source>Sessions: %1 / %2 / %3 Length: %4 / %5 / %6 Longest: %7 / %8 / %9</source>
        <translation>Sessões: %1 / %2 / %3 Duração: %4 / %5 / %6 Mais Longa: %7 / %8 / %9</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="970"/>
        <source>%1
Length: %3
Start: %2
</source>
        <translation>%1
Duração: %3
Início: %2
</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="972"/>
        <source>Mask On</source>
        <translation>Mascara Colocada</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="972"/>
        <source>Mask Off</source>
        <translation>Mascara Removida</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="983"/>
        <source>%1
Length: %3
Start: %2</source>
        <translation>%1
Duração: %3
Início: %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1155"/>
        <source>TTIA:</source>
        <translatorcomment>Tempo Total Em Apneia?</translatorcomment>
        <translation>TTIA:</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="1170"/>
        <source>
TTIA: %1</source>
        <translation>
TTIA: %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="683"/>
        <source>bpm</source>
        <translation>bpm</translation>
    </message>
    <message>
        <source>?</source>
        <translation type="vanished">?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="690"/>
        <source>Severity (0-1)</source>
        <translation>Severidade (0-1)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="694"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2889"/>
        <source>Error</source>
        <translation>Erro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="695"/>
        <source>Warning</source>
        <translation>Aviso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="698"/>
        <source>Please Note</source>
        <translation>Por Favor Note</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="702"/>
        <source>Graphs Switched Off</source>
        <translation>Gráficos Desativados</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="704"/>
        <source>Sessions Switched Off</source>
        <translation>Sessões Desativadas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="708"/>
        <source>&amp;Yes</source>
        <translation>&amp;Sim</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="709"/>
        <source>&amp;No</source>
        <translation>&amp;Não</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="710"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Cancelar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="711"/>
        <source>&amp;Destroy</source>
        <translation>&amp;Destruir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="712"/>
        <source>&amp;Save</source>
        <translation>&amp;Salvar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="714"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>BMI</source>
        <translation>IMC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="715"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="319"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="716"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>Zombie</source>
        <translation>Zumbi</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="717"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="204"/>
        <source>Pulse Rate</source>
        <translation>Taxa de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="719"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>Plethy</source>
        <translation>Pletis</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="720"/>
        <source>Pressure</source>
        <translation>Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="722"/>
        <source>Daily</source>
        <translation>Diário</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="723"/>
        <source>Profile</source>
        <translation>Perfil</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="724"/>
        <source>Overview</source>
        <translation>Visão-Geral</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="725"/>
        <source>Oximetry</source>
        <translation>Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="727"/>
        <source>Oximeter</source>
        <translation>Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="728"/>
        <source>Event Flags</source>
        <translation>Marcações de Evento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="731"/>
        <source>Default</source>
        <translation>Padrão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="734"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2810"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="105"/>
        <source>CPAP</source>
        <translation>CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="735"/>
        <source>BiPAP</source>
        <translation>BiPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="736"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2813"/>
        <source>Bi-Level</source>
        <translation>Bi-Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="737"/>
        <source>EPAP</source>
        <translation>EPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="738"/>
        <source>Min EPAP</source>
        <translation>EPAP Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="739"/>
        <source>Max EPAP</source>
        <translation>EPAP Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="740"/>
        <source>IPAP</source>
        <translation>IPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="743"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="106"/>
        <source>APAP</source>
        <translation>APAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="744"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2815"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="112"/>
        <source>ASV</source>
        <translation>ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="745"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="313"/>
        <source>AVAPS</source>
        <translation>AVAPS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="746"/>
        <source>ST/ASV</source>
        <translation>ST/ASV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="748"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2874"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2901"/>
        <source>Humidifier</source>
        <translation>Umidifcador</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="750"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>H</source>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="751"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>OA</source>
        <translation>OA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="752"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="167"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="754"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>CA</source>
        <translation>CA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="755"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>FL</source>
        <translation>FL</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="757"/>
        <source>LE</source>
        <translation>LE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="758"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="188"/>
        <source>EP</source>
        <translation>EP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="759"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="173"/>
        <source>VS</source>
        <translation>VS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="761"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>VS2</source>
        <translation>VS2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="762"/>
        <source>RERA</source>
        <translation>RERA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="763"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2801"/>
        <source>PP</source>
        <translation>PP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="764"/>
        <source>P</source>
        <translation>P</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="765"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>RE</source>
        <translation>RE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="766"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>NR</source>
        <translation>NR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="767"/>
        <source>NRI</source>
        <translation>NRI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="768"/>
        <source>O2</source>
        <translation>O2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="769"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2818"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>PC</source>
        <translation>PC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="770"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>UF1</source>
        <translation>UF1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="771"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>UF2</source>
        <translation>UF2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="772"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>UF3</source>
        <translation>UF3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="774"/>
        <source>PS</source>
        <translation>PS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="775"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>AHI</source>
        <translation>IAH</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="776"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="284"/>
        <source>RDI</source>
        <translation>IDR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="777"/>
        <source>AI</source>
        <translation>IA</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="778"/>
        <source>HI</source>
        <translation>IH</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="779"/>
        <source>UAI</source>
        <translatorcomment>Índice de Apneia Indeterminada</translatorcomment>
        <translation>IAI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="780"/>
        <source>CAI</source>
        <translation>IAC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="781"/>
        <source>FLI</source>
        <translatorcomment>Índice de Limitação de Fluxo</translatorcomment>
        <translation>FLI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="783"/>
        <source>REI</source>
        <translatorcomment>Índice de Eventos Respiratórios</translatorcomment>
        <translation>REI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="784"/>
        <source>EPI</source>
        <translation>EPI</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="826"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="741"/>
        <source>Min IPAP</source>
        <translation>IPAP Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="202"/>
        <source>App key:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="194"/>
        <source>Operating system:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="192"/>
        <source>Built with Qt %1 on %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="195"/>
        <source>Graphics Engine:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="196"/>
        <source>Graphics Engine type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="661"/>
        <source>Software Engine</source>
        <translation>Motor de Software</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="662"/>
        <source>ANGLE / OpenGLES</source>
        <translation>ANGLE / OpenGLES</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="663"/>
        <source>Desktop OpenGL</source>
        <translation>OpenGL Desktop</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="665"/>
        <source> m</source>
        <translation> m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="666"/>
        <source> cm</source>
        <translation> cm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="667"/>
        <source>in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="671"/>
        <source>kg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="674"/>
        <source>Minutes</source>
        <translation>Minutos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="675"/>
        <source>Seconds</source>
        <translation>Segundos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="676"/>
        <source>h</source>
        <translation>h</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="677"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="678"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="679"/>
        <source>ms</source>
        <translation>ms</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="680"/>
        <source>Events/hr</source>
        <translation>Eventos/hr</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="682"/>
        <source>Hz</source>
        <translation>Hz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="684"/>
        <source>l/min</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="685"/>
        <source>Litres</source>
        <translation>Litros</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="686"/>
        <source>ml</source>
        <translation>ml</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="687"/>
        <source>Breaths/min</source>
        <translation>Respirações/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="691"/>
        <source>Degrees</source>
        <translation>Graus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="696"/>
        <source>Information</source>
        <translation>Informação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="697"/>
        <source>Busy</source>
        <translation>Ocupado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="701"/>
        <source>Only Settings and Compliance Data Available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="703"/>
        <source>Summary Data Only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="742"/>
        <source>Max IPAP</source>
        <translation>IPAP Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="756"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="191"/>
        <source>SA</source>
        <translatorcomment>Apneia do Sono</translatorcomment>
        <translation>AS</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="786"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>PB</source>
        <translatorcomment>Respiração Periódica</translatorcomment>
        <translation>RP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="790"/>
        <source>IE</source>
        <translation>IE</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="791"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="260"/>
        <source>Insp. Time</source>
        <translation>Tempo de Insp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="792"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="257"/>
        <source>Exp. Time</source>
        <translation>Tempo de Exp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="793"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="263"/>
        <source>Resp. Event</source>
        <translation>Evento Resp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="794"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="266"/>
        <source>Flow Limitation</source>
        <translation>Limitação de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="795"/>
        <source>Flow Limit</source>
        <translation>Limite de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="796"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1022"/>
        <source>SensAwake</source>
        <translation>SensAwake</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="797"/>
        <source>Pat. Trig. Breath</source>
        <translation>Resp. por Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="798"/>
        <source>Tgt. Min. Vent</source>
        <translation>Vent. Mín. Alvo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="799"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="269"/>
        <source>Target Vent.</source>
        <translation>Vent Alvo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="800"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Minute Vent.</source>
        <translation>Vent. Minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="801"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="235"/>
        <source>Tidal Volume</source>
        <translation>Volume Tidal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="802"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Resp. Rate</source>
        <translation>Taxa de Resp.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="803"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2804"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Snore</source>
        <translation>Ronco</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="804"/>
        <source>Leak</source>
        <translation>Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="805"/>
        <source>Leaks</source>
        <translation>Vazamentos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="808"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>Total Leaks</source>
        <translation>Vazamentos Toais</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="809"/>
        <source>Unintentional Leaks</source>
        <translation>Vazamentos Não-Intencionais</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="810"/>
        <source>MaskPressure</source>
        <translation>PressãoMáscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="811"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="226"/>
        <source>Flow Rate</source>
        <translation>Taxa de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="812"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>Sleep Stage</source>
        <translation>Estágio do Sono</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="813"/>
        <source>Usage</source>
        <translation>Uso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="814"/>
        <source>Sessions</source>
        <translation>Sessões</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="815"/>
        <source>Pr. Relief</source>
        <translation>Alívio de Pr.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="700"/>
        <source>No Data Available</source>
        <translation>Nenhum Dado Disponível</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="817"/>
        <source>Bookmarks</source>
        <translation>Favoritos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="821"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2805"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2807"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="101"/>
        <source>Mode</source>
        <translation>Modo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="822"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="823"/>
        <source>Brand</source>
        <translation>Marca</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="824"/>
        <source>Serial</source>
        <translation>Serial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="825"/>
        <source>Series</source>
        <translation>Série</translation>
    </message>
    <message>
        <source>Machine</source>
        <translation type="vanished">Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="827"/>
        <source>Channel</source>
        <translation>Canal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="828"/>
        <source>Settings</source>
        <translation>Configurações</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="832"/>
        <source>Motion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="834"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="835"/>
        <source>DOB</source>
        <translation>Data Nasc.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="836"/>
        <source>Phone</source>
        <translation>Telefone</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="837"/>
        <source>Address</source>
        <translation>Endereço</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="838"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="839"/>
        <source>Patient ID</source>
        <translation>RG Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="840"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="842"/>
        <source>Bedtime</source>
        <translation>Hora de Dormir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="843"/>
        <source>Wake-up</source>
        <translation>Acordar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="844"/>
        <source>Mask Time</source>
        <translation>Tempo de Másc</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="845"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="117"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="194"/>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="125"/>
        <source>Unknown</source>
        <translation>Desconhecido</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="846"/>
        <source>None</source>
        <translation>Nenhum</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="847"/>
        <source>Ready</source>
        <translation>Pronto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="849"/>
        <source>First</source>
        <translation>Primeiro</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="850"/>
        <source>Last</source>
        <translation>Último</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="851"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Start</source>
        <translation>Início</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="852"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>End</source>
        <translation>Fim</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="856"/>
        <source>Yes</source>
        <translation>Sim</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="857"/>
        <source>No</source>
        <translation>Não</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="859"/>
        <source>Min</source>
        <translation>Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="860"/>
        <source>Max</source>
        <translation>Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="861"/>
        <source>Med</source>
        <translation>Méd</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="863"/>
        <source>Average</source>
        <translation>Média</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="864"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="235"/>
        <location filename="../oscar/SleepLib/common.cpp" line="865"/>
        <source>Avg</source>
        <translation>Méd</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSessionTimesChart.cpp" line="233"/>
        <location filename="../oscar/SleepLib/common.cpp" line="866"/>
        <source>W-Avg</source>
        <translation>Méd-Aco</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2694"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="852"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="225"/>
        <source>Getting Ready...</source>
        <translation>Aprontando-se...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="151"/>
        <source>Your %1 %2 (%3) generated data that OSCAR has never seen before.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="152"/>
        <source>The imported data may not be entirely accurate, so the developers would like a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make sure OSCAR is handling the data correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="161"/>
        <source>Non Data Capable Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="162"/>
        <source>Your %1 CPAP Device (Model %2) is unfortunately not a data capable model.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="163"/>
        <source>I&apos;m sorry to report that OSCAR can only track hours of use and very basic settings for this device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="175"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="442"/>
        <source>Device Untested</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="176"/>
        <source>Your %1 CPAP Device (Model %2) has not been tested yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="177"/>
        <source>It seems similar enough to other devices that it might work, but the developers would like a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make sure it works with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="185"/>
        <source>Device Unsupported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="186"/>
        <source>Sorry, your %1 CPAP Device (%2) is not supported yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="187"/>
        <source>The developers need a .zip copy of this device&apos;s SD card and matching clinician .pdf reports to make it work with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Non Data Capable Machine</source>
        <translation type="vanished">Máquina Sem Capacidade de Dados</translation>
    </message>
    <message>
        <source>Machine Unsupported</source>
        <translation type="vanished">Máquina Não Suportada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="877"/>
        <source>Scanning Files...</source>
        <translation>Vasculhando Arquivos...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="885"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="731"/>
        <location filename="../oscar/mainwindow.cpp" line="2311"/>
        <source>Importing Sessions...</source>
        <translation>Importando Sessões...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2748"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="368"/>
        <location filename="../oscar/mainwindow.cpp" line="734"/>
        <location filename="../oscar/mainwindow.cpp" line="2333"/>
        <source>Finishing up...</source>
        <translation>Finalizando...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/importcontext.cpp" line="150"/>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="92"/>
        <source>Untested Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2809"/>
        <source>CPAP-Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2811"/>
        <source>AutoCPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2812"/>
        <source>Auto-Trial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2814"/>
        <source>AutoBiLevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2816"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2817"/>
        <source>S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2819"/>
        <source>S/T - AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2820"/>
        <source>PC - AVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2835"/>
        <source>Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2847"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2849"/>
        <source>Flex Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2848"/>
        <source>Whether Flex settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2857"/>
        <source>Amount of time it takes to transition from EPAP to IPAP, the higher the number the slower the transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2863"/>
        <source>Rise Time Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2864"/>
        <source>Whether Rise Time settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2865"/>
        <source>Rise Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2888"/>
        <source>Passover</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2909"/>
        <source>Target Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2910"/>
        <source>PRS1 Humidifier Target Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2911"/>
        <source>Hum. Tgt Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2918"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2919"/>
        <source>Mask Resistance Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2920"/>
        <source>Mask Resist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2928"/>
        <source>Hose Diam.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2931"/>
        <source>15mm</source>
        <translation>15mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2936"/>
        <source>Tubing Type Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2937"/>
        <source>Whether tubing type settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2938"/>
        <source>Tube Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2945"/>
        <source>Mask Resistance Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2946"/>
        <source>Whether mask resistance settings are available to you.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2947"/>
        <source>Mask Res. Lock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2955"/>
        <source>A few breaths automatically starts device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2964"/>
        <source>Device automatically switches off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2973"/>
        <source>Whether or not device allows Mask checking.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2990"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2992"/>
        <source>Ramp Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2991"/>
        <source>Type of ramp curve to use.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2994"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2995"/>
        <source>SmartRamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2996"/>
        <source>Ramp+</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3000"/>
        <source>Backup Breath Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3001"/>
        <source>The kind of backup breath rate in use: none (off), automatic, or fixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3002"/>
        <source>Breath Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3006"/>
        <source>Fixed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3010"/>
        <source>Fixed Backup Breath BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3011"/>
        <source>Minimum breaths per minute (BPM) below which a timed breath will be initiated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3012"/>
        <source>Breath BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3017"/>
        <source>Timed Inspiration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3018"/>
        <source>The time that a timed breath will provide IPAP before transitioning to EPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3019"/>
        <source>Timed Insp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3024"/>
        <source>Auto-Trial Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3026"/>
        <source>Auto-Trial Dur.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3031"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3033"/>
        <source>EZ-Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3032"/>
        <source>Whether or not EZ-Start is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3040"/>
        <source>Variable Breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3041"/>
        <source>UNCONFIRMED: Possibly variable breathing, which are periods of high deviation from the peak inspiratory flow trend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3050"/>
        <source>A period during a session where the device could not detect flow.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3064"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3066"/>
        <source>Peak Flow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3065"/>
        <source>Peak flow during a 2-minute interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2930"/>
        <source>22mm</source>
        <translation>22mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="868"/>
        <source>Backing Up Files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="943"/>
        <source>model %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="946"/>
        <source>unknown model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2823"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2825"/>
        <source>Flex Mode</source>
        <translation>Flex Mode</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2824"/>
        <source>PRS1 pressure relief mode.</source>
        <translation>Modo PRS1 de alívio de pressão.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2828"/>
        <source>C-Flex</source>
        <translation>C-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2829"/>
        <source>C-Flex+</source>
        <translation>C-Flex+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2830"/>
        <source>A-Flex</source>
        <translation>A-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2831"/>
        <source>P-Flex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2832"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2856"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2858"/>
        <source>Rise Time</source>
        <translation>Tempo de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2833"/>
        <source>Bi-Flex</source>
        <translation>Bi-Flex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2839"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2841"/>
        <source>Flex Level</source>
        <translation>Flex Level</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2840"/>
        <source>PRS1 pressure relief setting.</source>
        <translation>Configuração PRS1 alívio de pressão.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2872"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="141"/>
        <source>Humidifier Status</source>
        <translation>Estado de Umidificador</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2873"/>
        <source>PRS1 humidifier connected?</source>
        <translation>Umidificador PRS1 conectado?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2876"/>
        <source>Disconnected</source>
        <translation>Disconectado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2877"/>
        <source>Connected</source>
        <translation>Conectado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2881"/>
        <source>Humidification Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2882"/>
        <source>PRS1 Humidification Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2883"/>
        <source>Humid. Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2885"/>
        <source>Fixed (Classic)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2886"/>
        <source>Adaptive (System One)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2887"/>
        <source>Heated Tube</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2893"/>
        <source>Tube Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2894"/>
        <source>PRS1 Heated Tube Temperature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2895"/>
        <source>Tube Temp.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2902"/>
        <source>PRS1 Humidifier Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2926"/>
        <source>Hose Diameter</source>
        <translation>Diâmetro da Traquéia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2927"/>
        <source>Diameter of primary CPAP hose</source>
        <translation>Diâmetro da traquéia do CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2932"/>
        <source>12mm</source>
        <translation>12mm</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2954"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2956"/>
        <source>Auto On</source>
        <translation>Auto Ligar</translation>
    </message>
    <message>
        <source>A few breaths automatically starts machine</source>
        <translation type="vanished">Algumas respirações automaticamente ligam a máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2963"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2965"/>
        <source>Auto Off</source>
        <translation>Auto Desligar</translation>
    </message>
    <message>
        <source>Machine automatically switches off</source>
        <translation type="vanished">Máquina desliga automaticamente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2972"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2974"/>
        <source>Mask Alert</source>
        <translation>Alerta de Máscara</translation>
    </message>
    <message>
        <source>Whether or not machine allows Mask checking.</source>
        <translation type="vanished">Se a máquina permite verificação da máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2981"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2983"/>
        <source>Show AHI</source>
        <translation>Mostrar IAH</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2982"/>
        <source>Whether or not device shows AHI via built-in display.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3025"/>
        <source>The number of days in the Auto-CPAP trial period, after which the device will revert to CPAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3049"/>
        <source>Breathing Not Detected</source>
        <translation>Respiração Não Detectada</translation>
    </message>
    <message>
        <source>A period during a session where the machine could not detect flow.</source>
        <translation type="vanished">Um período durante a sesão onde a máquina não pôde detectar fluxo.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3051"/>
        <source>BND</source>
        <translatorcomment>Respiração Não Detectada</translatorcomment>
        <translation>RND</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3056"/>
        <source>Timed Breath</source>
        <translation>Respiração Cronometrada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3057"/>
        <source>Machine Initiated Breath</source>
        <translation>Respiração Iniciada pela Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3058"/>
        <source>TB</source>
        <translatorcomment>Respiração Cronometrada</translatorcomment>
        <translation>RC</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="37"/>
        <source>Windows User</source>
        <translation>Utilizador Windows</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>Using </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="202"/>
        <source>, found SleepyHead -
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/preferences.cpp" line="203"/>
        <source>You must run the OSCAR Migration Tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;Your old machine data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation type="vanished">&lt;i&gt;Seus dados de máquina antigos devem ser regenerados, desde que esse recurso de backup não tenha sido desativado nas preferências durante uma importação de dados anterior.&lt;/i&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="482"/>
        <source>Launching Windows Explorer failed</source>
        <translation>Execução do Windows Explorer falhou</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="483"/>
        <source>Could not find explorer.exe in path to launch Windows Explorer.</source>
        <translation>Não foi possível encontrar o explorer.exe no caminho para iniciar o Windows Explorer.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="548"/>
        <source>&lt;b&gt;OSCAR maintains a backup of your devices data card that it uses for this purpose.&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="549"/>
        <source>&lt;i&gt;Your old device data should be regenerated provided this backup feature has not been disabled in preferences during a previous data import.&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="552"/>
        <source>OSCAR does not yet have any automatic card backups stored for this device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This means you will need to import this machine data again afterwards from your own backups or data card.</source>
        <translation type="vanished">Isso significa que você precisará importar os dados da máquina novamente a partir de seus próprios backups ou cartão de dados.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="556"/>
        <source>Important:</source>
        <translation>Importante:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="557"/>
        <source>If you are concerned, click No to exit, and backup your profile manually, before starting OSCAR again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="558"/>
        <source>Are you ready to upgrade, so you can run the new version of OSCAR?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="561"/>
        <source>Device Database Changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="572"/>
        <source>Sorry, the purge operation failed, which means this version of OSCAR can&apos;t start.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="573"/>
        <source>The device data folder needs to be removed manually.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="591"/>
        <source>Would you like to switch on automatic backups, so next time a new version of OSCAR needs to do so, it can rebuild from these?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="598"/>
        <source>OSCAR will now start the import wizard so you can reinstall your %1 data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="608"/>
        <source>OSCAR will now exit, then (attempt to) launch your computers file manager so you can manually back your profile up:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="610"/>
        <source>Use your file manager to make a copy of your profile directory, then afterwards, restart OSCAR and complete the upgrade process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Machine Database Changes</source>
        <translation type="vanished">Alterações no Banco de Dados da Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="535"/>
        <source>OSCAR %1 needs to upgrade its database for %2 %3 %4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="553"/>
        <source>This means you will need to import this device data again afterwards from your own backups or data card.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="556"/>
        <source>Once you upgrade, you &lt;font size=+1&gt;cannot&lt;/font&gt; use this profile with the previous version anymore.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The machine data folder needs to be removed manually.</source>
        <translation type="vanished">A pasta de dados da máquina precisa ser removida manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="574"/>
        <source>This folder currently resides at the following location:</source>
        <translation>Essa pasta reside atualmente na localização seguinte:</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/profiles.cpp" line="582"/>
        <source>Rebuilding from %1 Backup</source>
        <translation>Recompilando %1 do backup</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="193"/>
        <source>or CANCEL to skip migration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="208"/>
        <source>You cannot use this folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="223"/>
        <source>Migrating </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="223"/>
        <source> files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="224"/>
        <source>from </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="224"/>
        <source>to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="342"/>
        <source>OSCAR crashed due to an incompatibility with your graphics hardware.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="343"/>
        <source>To resolve this, OSCAR has reverted to a slower but more compatible method of drawing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="510"/>
        <source>OSCAR will set up a folder for your data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="513"/>
        <source>We suggest you use this folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="514"/>
        <source>Click Ok to accept this, or No if you want to use a different folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="525"/>
        <source>Next time you run OSCAR, you will be asked again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="565"/>
        <source>Migrate SleepyHead or OSCAR Data?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="566"/>
        <source>On the next screen OSCAR will ask you to select a folder with SleepyHead or OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="567"/>
        <source>Click [OK] to go to the next screen or [No] if you do not wish to use any SleepyHead or OSCAR data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="576"/>
        <source>Unable to create the OSCAR data folder at</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="586"/>
        <source>Unable to write to OSCAR data directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="587"/>
        <source>Error code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="588"/>
        <source>OSCAR cannot continue and is exiting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="599"/>
        <source>Unable to write to debug log. You can still use the debug pane (Help/Troubleshooting/Show Debug Pane) but the debug log will not be written to disk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="658"/>
        <source>Version &quot;%1&quot; is invalid, cannot continue!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="667"/>
        <source>The version of OSCAR you are running (%1) is OLDER than the one used to create this data (%2).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="520"/>
        <source>Choose or create a new folder for OSCAR data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="192"/>
        <source>Choose the SleepyHead or OSCAR data folder to migrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="207"/>
        <source>The folder you chose does not contain valid SleepyHead or OSCAR data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="511"/>
        <source>If you have been using SleepyHead or an older version of OSCAR,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="512"/>
        <source>OSCAR can copy your old data to this folder later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="524"/>
        <source>As you did not select a data folder, OSCAR will exit.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="536"/>
        <source>The folder you chose is not empty, nor does it already contain valid OSCAR data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="557"/>
        <source>Data directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="671"/>
        <source>It is likely that doing this will cause data corruption, are you sure you want to do this?</source>
        <translation>É provável que isso cause corrupção de dados. Tem certeza de que deseja fazer isso?</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="693"/>
        <source>Question</source>
        <translation>Pergunta</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="523"/>
        <location filename="../oscar/main.cpp" line="575"/>
        <location filename="../oscar/main.cpp" line="590"/>
        <source>Exiting</source>
        <translation>Encerrando</translation>
    </message>
    <message>
        <location filename="../oscar/main.cpp" line="537"/>
        <source>Are you sure you want to use this folder?</source>
        <translation>Tem certeza de que deseja usar esta pasta?</translation>
    </message>
    <message>
        <source>Don&apos;t forget to place your datacard back in your CPAP machine</source>
        <translation type="vanished">Não se esqueça de colocar o seu cartão de volta na sua máquina de CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="284"/>
        <source>OSCAR Reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="284"/>
        <source>Don&apos;t forget to place your datacard back in your CPAP device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="468"/>
        <source>You can only work with one instance of an individual OSCAR profile at a time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="469"/>
        <source>If you are using cloud storage, make sure OSCAR is closed and syncing has completed first on the other computer before proceeding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="482"/>
        <source>Loading profile &quot;%1&quot;...</source>
        <translation>Carregando perfil &quot;%1&quot;...</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="948"/>
        <source>Chromebook file system detected, but no removable device found
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="949"/>
        <source>You must share your SD card with Linux using the ChromeOS Files program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2112"/>
        <source>Recompressing Session Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2633"/>
        <source>Please select a location for your zip other than the data card itself!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="2680"/>
        <location filename="../oscar/mainwindow.cpp" line="2730"/>
        <location filename="../oscar/mainwindow.cpp" line="2789"/>
        <source>Unable to create zip!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1201"/>
        <source>Are you sure you want to reset all your channel colors and settings to defaults?</source>
        <translation>Tem certeza de que deseja redefinir todas as suas configurações de cores e canais para os padrões?</translation>
    </message>
    <message>
        <location filename="../oscar/preferencesdialog.cpp" line="1254"/>
        <source>Are you sure you want to reset all your waveform channel colors and settings to defaults?</source>
        <translation>Tem certeza de que deseja redefinir todas as cores e configurações do seu canal de forma de onda para os padrões?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="40"/>
        <source>There are no graphs visible to print</source>
        <translation>Não há gráficos visíveis para imprimir</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="55"/>
        <source>Would you like to show bookmarked areas in this report?</source>
        <translation>Gostaria de mostrar áreas favoritadas neste relatório?</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="99"/>
        <source>Printing %1 Report</source>
        <translation>Imprimindo Relatório %1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="133"/>
        <source>%1 Report</source>
        <translation>Relatório %1</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="191"/>
        <source>: %1 hours, %2 minutes, %3 seconds
</source>
        <translation>: %1 horas, %2 minutos, %3 segundos
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="244"/>
        <source>RDI	%1
</source>
        <translation>IDR	%1
</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="246"/>
        <source>AHI	%1
</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="279"/>
        <source>AI=%1 HI=%2 CAI=%3 </source>
        <translation>AI=%1 HI=%2 CAI=%3 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="285"/>
        <source>REI=%1 VSI=%2 FLI=%3 PB/CSR=%4%%</source>
        <translation>REI=%1 VSI=%2 FLI=%3 RP/RCS=%4%%</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="289"/>
        <source>UAI=%1 </source>
        <translation>IAI=%1 </translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="291"/>
        <source>NRI=%1 LKI=%2 EPI=%3</source>
        <translation>NRI=%1 LKI=%2 EPI=%3</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="294"/>
        <source>AI=%1 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="362"/>
        <source>Reporting from %1 to %2</source>
        <translation>Relatando de %1 a %2</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="427"/>
        <source>Entire Day&apos;s Flow Waveform</source>
        <translation>Forma de Onda de Fluxo do Dia Todo</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="429"/>
        <source>Current Selection</source>
        <translation>Seleção Atual</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="439"/>
        <source>Entire Day</source>
        <translation>Dia Inteiro</translation>
    </message>
    <message>
        <location filename="../oscar/reports.cpp" line="564"/>
        <source>Page %1 of %2</source>
        <translation>Página %1 de %2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jan</source>
        <translation>Jan</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Feb</source>
        <translation>Fev</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Mar</source>
        <translation>Mar</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Apr</source>
        <translation>Abr</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>May</source>
        <translation>ai</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="66"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="398"/>
        <source>Jun</source>
        <translation>Jun</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Jul</source>
        <translation>Jul</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Aug</source>
        <translation>Ago</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Sep</source>
        <translation>Set</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Oct</source>
        <translation>Out</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Nov</source>
        <translation>Nov</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="67"/>
        <location filename="../oscar/Graphs/gXAxis.cpp" line="399"/>
        <source>Dec</source>
        <translation>Dez</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="379"/>
        <source>Events</source>
        <translation>Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="377"/>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="380"/>
        <source>Duration</source>
        <translation>Duração</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineOverlay.cpp" line="394"/>
        <source>(% %1 in events)</source>
        <translation>(% %1 em eventos)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="134"/>
        <source>Therapy Pressure</source>
        <translation>Pressão da Terapia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="135"/>
        <source>Inspiratory Pressure</source>
        <translation>Pressão Inspiratória</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="136"/>
        <source>Lower Inspiratory Pressure</source>
        <translation>Pressão Inferior Respiratória</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="137"/>
        <source>Higher Inspiratory Pressure</source>
        <translation>Pressão Superior Respiratória</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="138"/>
        <source>Expiratory Pressure</source>
        <translation>Pressão Expiratória</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="139"/>
        <source>Lower Expiratory Pressure</source>
        <translation>Pressão Expiratória Inferior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="140"/>
        <source>Higher Expiratory Pressure</source>
        <translation>Pressão Expiratória Superior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="141"/>
        <source>Pressure Support</source>
        <translation>Suporte de Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>PS Min</source>
        <translation>PS Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="142"/>
        <source>Pressure Support Minimum</source>
        <translation>Mínima Pressão de Suporte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>PS Max</source>
        <translation>PS Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="143"/>
        <source>Pressure Support Maximum</source>
        <translation>Máxima Pressão de Suporte</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Min Pressure</source>
        <translation>Pressão Mín</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Minimum Therapy Pressure</source>
        <translation>Mínima Pressão de Terapia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Max Pressure</source>
        <translation>Pressão Máx</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Maximum Therapy Pressure</source>
        <translation>Máxima Pressão de Terapia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Ramp Time</source>
        <translation>Tempo de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="146"/>
        <source>Ramp Delay Period</source>
        <translation>Período de Atraso de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Ramp Pressure</source>
        <translation>Pressão de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="147"/>
        <source>Starting Ramp Pressure</source>
        <translation>Pressão de Rampa Inicial</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Ramp Event</source>
        <translation>Evento de Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="197"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1039"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="149"/>
        <source>Ramp</source>
        <translation>Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>Vibratory Snore (VS2) </source>
        <translation>Ronco Vibratório (VS2) </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="263"/>
        <source>A ResMed data item: Trigger Cycle Event</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="297"/>
        <source>Mask On Time</source>
        <translation>Tempo com Máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="297"/>
        <source>Time started according to str.edf</source>
        <translation>Tempo iniciado de acordo com str.edf</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>Summary Only</source>
        <translation>Apenas Resumo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>An apnea where the airway is open</source>
        <translation>Uma apneia em que a via aérea está aberta</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>Cheyne Stokes Respiration (CSR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>Periodic Breathing (PB)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="159"/>
        <source>Clear Airway (CA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>An apnea caused by airway obstruction</source>
        <translation>Uma apneia causada por uma bostrução de via aérea</translation>
    </message>
    <message>
        <source>Hypopnea</source>
        <translation type="vanished">Hipoapneia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>A partially obstructed airway</source>
        <translation>Uma via aérea parcialmente obstruída</translation>
    </message>
    <message>
        <source>Unclassified Apnea</source>
        <translation type="vanished">Apneia Indeterminada</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="753"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="165"/>
        <source>UA</source>
        <translation>AI</translation>
    </message>
    <message>
        <source>Vibratory Snore</source>
        <translation type="vanished">Ronco Vibratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="173"/>
        <source>A vibratory snore</source>
        <translation>Um ronco vibratório</translation>
    </message>
    <message>
        <source>A vibratory snore as detcted by a System One machine</source>
        <translation type="vanished">Um ronco vibratório como detectado por uma máquina System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2799"/>
        <source>Pressure Pulse</source>
        <translation>Pulso de Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2800"/>
        <source>A pulse of pressure &apos;pinged&apos; to detect a closed airway.</source>
        <translation>Um pulseo de pressão &apos;pingado&apos; para detectar uma via aérea fechada.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="806"/>
        <source>Large Leak</source>
        <translation>Grande Vazamento</translation>
    </message>
    <message>
        <source>A large mask leak affecting machine performance.</source>
        <translation type="vanished">Um grande vazamento de máscara afetando o desempenho da máquina.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="807"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="182"/>
        <source>LL</source>
        <translation>GV</translation>
    </message>
    <message>
        <source>Non Responding Event</source>
        <translation type="vanished">Um Evento Não Respondendo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>A type of respiratory event that won&apos;t respond to a pressure increase.</source>
        <translation>Um tipo de evento que não irá responder a um aumento na pressão.</translation>
    </message>
    <message>
        <source>Expiratory Puff</source>
        <translation type="vanished">Sopro Expiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="188"/>
        <source>Intellipap event where you breathe out your mouth.</source>
        <translation>Evento Intellipap no qual você expira pela boca.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="191"/>
        <source>SensAwake feature will reduce pressure when waking is detected.</source>
        <translation>Recursos SensAwake reduzirá a pressão quando caminhar é detectado.</translation>
    </message>
    <message>
        <source>User Flag #1</source>
        <translation type="vanished">Marca de Utilizador #1</translation>
    </message>
    <message>
        <source>User Flag #2</source>
        <translation type="vanished">Marca de Utilizador #2</translation>
    </message>
    <message>
        <source>User Flag #3</source>
        <translation type="vanished">Marca de Utilizador #3</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="204"/>
        <source>Heart rate in beats per minute</source>
        <translation>Taxa cardíaca em batimentos por minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="209"/>
        <source>Blood-oxygen saturation percentage</source>
        <translation>Porcentagem de saturação de oxigênio no sangue</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>Plethysomogram</source>
        <translation>Pletismograma</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="213"/>
        <source>An optical Photo-plethysomogram showing heart rhythm</source>
        <translation>Um pletismograma foto-óptico mostrando o ritmo cardíaco</translation>
    </message>
    <message>
        <source>Pulse Change</source>
        <translation type="vanished">Mudança no pulso</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>A sudden (user definable) change in heart rate</source>
        <translation>Uma mudança brusca (definível pelo utilizador) na taxa cardíaca</translation>
    </message>
    <message>
        <source>SpO2 Drop</source>
        <translation type="vanished">Queda de SpO2</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>A sudden (user definable) drop in blood oxygen saturation</source>
        <translation>Uma quebra brusca (definível pelo utilizador) na saturação do sangue</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>SD</source>
        <translation>QB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="226"/>
        <source>Breathing flow rate waveform</source>
        <translation>Forma de onda da taxa de fluxo respiratório</translation>
    </message>
    <message>
        <source>L/min</source>
        <translation type="vanished">L/min</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="229"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="232"/>
        <source>Mask Pressure</source>
        <translation>Pressão de Máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="235"/>
        <source>Amount of air displaced per breath</source>
        <translation>Quantidade de ar deslocado por respiração</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="238"/>
        <source>Graph displaying snore volume</source>
        <translation>Gráfico mostrando o volume de ronco</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Minute Ventilation</source>
        <translation>Ventilação por Minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="241"/>
        <source>Amount of air displaced per minute</source>
        <translation>Quantidade de ar deslocado por minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Respiratory Rate</source>
        <translation>Taxa Respiratória</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="244"/>
        <source>Rate of breaths per minute</source>
        <translation>Taxa de respirações por minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Patient Triggered Breaths</source>
        <translation>Respirações Iniciadas pelo Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Percentage of breaths triggered by patient</source>
        <translation>Porcentagem de respirações iniciadas pelo paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="247"/>
        <source>Pat. Trig. Breaths</source>
        <translation>Resp. Inic. Paciente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="250"/>
        <source>Leak Rate</source>
        <translation>Taxa de Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="250"/>
        <source>Rate of detected mask leakage</source>
        <translation>Taxa do vazamento detectado na máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="254"/>
        <source>I:E Ratio</source>
        <translation>Taxa I:E</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="254"/>
        <source>Ratio between Inspiratory and Expiratory time</source>
        <translation>Taxa entre tempo inspiratório e expiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="689"/>
        <source>ratio</source>
        <translation>taxa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="144"/>
        <source>Pressure Min</source>
        <translation>Mín Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="145"/>
        <source>Pressure Max</source>
        <translation>Máx Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Pressure Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="150"/>
        <source>Pressure Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>IPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="151"/>
        <source>IPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>EPAP Set</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="152"/>
        <source>EPAP Setting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cheyne Stokes Respiration</source>
        <translation type="vanished">Respiração Cheyne Stokes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>An abnormal period of Cheyne Stokes Respiration</source>
        <translation>Um período anormal de respiração Cheyne Stokes</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="785"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="155"/>
        <source>CSR</source>
        <translation>RCS</translation>
    </message>
    <message>
        <source>Periodic Breathing</source>
        <translation type="vanished">Respiração Periódica</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="157"/>
        <source>An abnormal period of Periodic Breathing</source>
        <translation>Um período anormal de respiração</translation>
    </message>
    <message>
        <source>Clear Airway</source>
        <translation type="vanished">Via Aérea Livre</translation>
    </message>
    <message>
        <source>Obstructive</source>
        <translation type="vanished">Obstrutiva</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="165"/>
        <source>An apnea that couldn&apos;t be determined as Central or Obstructive.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>A restriction in breathing from normal, causing a flattening of the flow waveform.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>Respiratory Effort Related Arousal: An restriction in breathing that causes an either an awakening or sleep disturbance.</source>
        <translation>Excitação Relacionada ao Esforço Respiratório: Uma restrição na respiração que causa um despertar ou distúrbio do sono.</translation>
    </message>
    <message>
        <source>Leak Flag</source>
        <translation type="vanished">Marcação Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>LF</source>
        <translation>MV</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>A user definable event detected by OSCAR&apos;s flow waveform processor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>Perfusion Index</source>
        <translation>Índice de Perfusão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>A relative assessment of the pulse strength at the monitoring site</source>
        <translation>Uma avaliação relativa da força de pulso no lugar de monitoramente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="216"/>
        <source>Perf. Index %</source>
        <translation>Índice Perf. %</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="232"/>
        <source>Mask Pressure (High frequency)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="257"/>
        <source>Expiratory Time</source>
        <translation>Tempo Expiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="257"/>
        <source>Time taken to breathe out</source>
        <translation>Tempo usado para expirar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="260"/>
        <source>Inspiratory Time</source>
        <translation>Tempo Inspiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="260"/>
        <source>Time taken to breathe in</source>
        <translation>Tempo usado para inspirar</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="263"/>
        <source>Respiratory Event</source>
        <translation>Evento Respiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="266"/>
        <source>Graph showing severity of flow limitations</source>
        <translation>Gráfico mostrando a severidade de limitações de fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="266"/>
        <source>Flow Limit.</source>
        <translation>Limite de Fluxo.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="269"/>
        <source>Target Minute Ventilation</source>
        <translation>Alvo Ventilações Minuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>Maximum Leak</source>
        <translation>Vazamento Máximo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>The maximum rate of mask leakage</source>
        <translation>A taxa máxima de vazamento da máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="272"/>
        <source>Max Leaks</source>
        <translation>Vazamentos Máx</translation>
    </message>
    <message>
        <source>Apnea Hypopnea Index</source>
        <translation type="vanished">Índice de Apneia Hipo-apneia</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>Graph showing running AHI for the past hour</source>
        <translation>Gráfico mostrando IAH na hora precedente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>Total Leak Rate</source>
        <translation>Taxa Total de Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="278"/>
        <source>Detected mask leakage including natural Mask leakages</source>
        <translation>Vazamento de máscara detectado incluindo vazamentos naturais de máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="281"/>
        <source>Median Leak Rate</source>
        <translation>Taxa Mediana de Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="281"/>
        <source>Median rate of detected mask leakage</source>
        <translation>Taxa mediana de vazamento detectado na máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="281"/>
        <source>Median Leaks</source>
        <translation>Vazamentos Medianos</translation>
    </message>
    <message>
        <source>Respiratory Disturbance Index</source>
        <translation type="vanished">Índice de Distúrbio Respiratório</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="284"/>
        <source>Graph showing running RDI for the past hour</source>
        <translation>Gráfico Mostrando o IDR na hora precedente</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="294"/>
        <source>Movement</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="294"/>
        <source>Movement detector</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="300"/>
        <source>CPAP Session contains summary data only</source>
        <translation>Sessão CPAP contém apenas dados resumidos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2806"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>PAP Mode</source>
        <translation>Modo PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="121"/>
        <source>Couldn&apos;t parse Channels.xml, OSCAR cannot continue and is exiting.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="161"/>
        <source>Obstructive Apnea (OA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="163"/>
        <source>Hypopnea (H)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="165"/>
        <source>Unclassified Apnea (UA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="167"/>
        <source>Apnea (A)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="167"/>
        <source>An apnea reportred by your CPAP device.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="169"/>
        <source>Flow Limitation (FL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="171"/>
        <source>RERA (RE)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="173"/>
        <source>Vibratory Snore (VS)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="175"/>
        <source>A vibratory snore as detcted by a System One device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <source>Leak Flag (LF)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="178"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="182"/>
        <source>A large mask leak affecting device performance.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="182"/>
        <source>Large Leak (LL)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="185"/>
        <source>Non Responding Event (NR)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="188"/>
        <source>Expiratory Puff (EP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="191"/>
        <source>SensAwake (SA)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="194"/>
        <source>User Flag #1 (UF1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="197"/>
        <source>User Flag #2 (UF2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="200"/>
        <source>User Flag #3 (UF3)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="219"/>
        <source>Pulse Change (PC)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="222"/>
        <source>SpO2 Drop (SD)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="275"/>
        <source>Apnea Hypopnea Index (AHI)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="284"/>
        <source>Respiratory Disturbance Index (RDI)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="303"/>
        <source>PAP Device Mode</source>
        <translation>Modo Aparelho PAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="307"/>
        <source>APAP (Variable)</source>
        <translation>APAP (Variável)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="311"/>
        <source>ASV (Fixed EPAP)</source>
        <translation>ASV (EPAP Fixo)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="312"/>
        <source>ASV (Variable EPAP)</source>
        <translation>ASV (EPAP Variável)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="320"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="320"/>
        <source>Physical Height</source>
        <translation>Altura Física</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Notes</source>
        <translation>Notas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="321"/>
        <source>Bookmark Notes</source>
        <translation>Notas de Favorito</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="323"/>
        <source>Body Mass Index</source>
        <translation>Índice de Massa Corporal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="324"/>
        <source>How you feel (0 = like crap, 10 = unstoppable)</source>
        <translation>Como se sente (0 = como lixo, 10 = imparável)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="325"/>
        <source>Bookmark Start</source>
        <translation>Começo do Favorito</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="326"/>
        <source>Bookmark End</source>
        <translation>Fim do Favorito</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="327"/>
        <source>Last Updated</source>
        <translation>Última Atualização</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Journal Notes</source>
        <translation>Notas de Diário</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="328"/>
        <source>Journal</source>
        <translation>Diário</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="335"/>
        <source>1=Awake 2=REM 3=Light Sleep 4=Deep Sleep</source>
        <translation>1=Acordado 2=REM 3=Sono Leve 4=Sono Profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>Brain Wave</source>
        <translation>Onda Cerebral</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="337"/>
        <source>BrainWave</source>
        <translation>OndaCerebral</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Awakenings</source>
        <translation>Despertares</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="338"/>
        <source>Number of Awakenings</source>
        <translation>Número de Despertares</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>Morning Feel</source>
        <translation>Sensação Matutina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="339"/>
        <source>How you felt in the morning</source>
        <translation>Como se sentiu na manhã</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time Awake</source>
        <translation>Tempo Acordado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="340"/>
        <source>Time spent awake</source>
        <translation>Tempo gasto acordado</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time In REM Sleep</source>
        <translation>Tempo No Sono REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time spent in REM Sleep</source>
        <translation>Tempo gasto no sono REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="341"/>
        <source>Time in REM Sleep</source>
        <translation>Tempo no Sono REM</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time In Light Sleep</source>
        <translation>Tempo Em Sono Leve</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time spent in light sleep</source>
        <translation>Tempo gasto em sono leve</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="342"/>
        <source>Time in Light Sleep</source>
        <translation>Tempo em Sono Leve</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Time In Deep Sleep</source>
        <translation>Tempo Em Sono Profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Time spent in deep sleep</source>
        <translation>Tempo gasto em sono profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="343"/>
        <source>Time in Deep Sleep</source>
        <translation>Tempo em Sono Profundo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Time to Sleep</source>
        <translation>Tempo para Dormir</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="344"/>
        <source>Time taken to get to sleep</source>
        <translation>Tempo exigido para conseguir adormecer</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="345"/>
        <source>Zeo ZQ</source>
        <translation>Zeo ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="345"/>
        <source>Zeo sleep quality measurement</source>
        <translation>Medição Zeo de qualidade do sono</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="345"/>
        <source>ZEO ZQ</source>
        <translation>ZEO ZQ</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="356"/>
        <source>Debugging channel #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="356"/>
        <source>Test #1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="356"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="357"/>
        <source>For internal use only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="357"/>
        <source>Debugging channel #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="357"/>
        <source>Test #2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="775"/>
        <source>Zero</source>
        <translation>Zero</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="778"/>
        <source>Upper Threshold</source>
        <translation>Limite Superior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="781"/>
        <source>Lower Threshold</source>
        <translation>Limite Inferior</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="831"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="288"/>
        <source>Orientation</source>
        <translation>Orientação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="288"/>
        <source>Sleep position in degrees</source>
        <translation>Posição de sono em graus</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="830"/>
        <location filename="../oscar/SleepLib/schema.cpp" line="291"/>
        <source>Inclination</source>
        <translation>Inclinação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="291"/>
        <source>Upright angle in degrees</source>
        <translation>Ângulo na vertical em graus</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="998"/>
        <source>Days: %1</source>
        <translation>Dias: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1002"/>
        <source>Low Usage Days: %1</source>
        <translation>Dias de Pouco Uso: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1003"/>
        <source>(%1% compliant, defined as &gt; %2 hours)</source>
        <translation>(%1% obervância, definida como &gt; %2 horas)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1120"/>
        <source>(Sess: %1)</source>
        <translation>(Sess: %1)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1128"/>
        <source>Bedtime: %1</source>
        <translation>Hora de cama: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1130"/>
        <source>Waketime: %1</source>
        <translation>Hora acordado: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gSummaryChart.cpp" line="1242"/>
        <source>(Summary Only)</source>
        <translation>(Apenas Resumod)</translation>
    </message>
    <message>
        <location filename="../oscar/mainwindow.cpp" line="467"/>
        <source>There is a lockfile already present for this profile &apos;%1&apos;, claimed on &apos;%2&apos;.</source>
        <translation>Existe um arquivo de bloqueio já presente para este perfil &apos;%1&apos;, reivindicado em &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="308"/>
        <source>Fixed Bi-Level</source>
        <translation>Bi-Level Fixo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="309"/>
        <source>Auto Bi-Level (Fixed PS)</source>
        <translation>Auto Bi-Level (PS Fixa)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/schema.cpp" line="310"/>
        <source>Auto Bi-Level (Variable PS)</source>
        <translation>Auto Bi-Level (PS Variável)</translation>
    </message>
    <message>
        <source>99.5%</source>
        <translation type="obsolete">90% {99.5%?}</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1534"/>
        <source>varies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1568"/>
        <source>n/a</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1585"/>
        <source>Fixed %1 (%2)</source>
        <translation>%1 (%2) Fixa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1588"/>
        <source>Min %1 Max %2 (%3)</source>
        <translation>Mín %1 Máx %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1592"/>
        <source>EPAP %1 IPAP %2 (%3)</source>
        <translation>EPAP %1 IPAP %2 (%3)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1596"/>
        <source>PS %1 over %2-%3 (%4)</source>
        <translation>PS %1 sobre %2-%3 (%4)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1601"/>
        <location filename="../oscar/SleepLib/day.cpp" line="1610"/>
        <source>Min EPAP %1 Max IPAP %2 PS %3-%4 (%5)</source>
        <translation>EPAP Mín %1 IPAP Máx %2 PS %3-%4 (%5)</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1606"/>
        <source>EPAP %1 PS %2-%3 (%4)</source>
        <translation type="unfinished">EPAP %1 PS %2-%3 (%6) {1 ?} {2-%3 ?} {4)?}</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1621"/>
        <source>EPAP %1 IPAP %2-%3 (%4)</source>
        <translation type="unfinished">EPAP %1 IPAP %2 (%3) {1 ?} {2-%3 ?} {4)?}</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/day.cpp" line="1627"/>
        <source>EPAP %1-%2 IPAP %3-%4 (%5)</source>
        <translation type="unfinished">EPAP %1 IPAP %2 (%3) {1-%2 ?} {3-%4 ?} {5)?}</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="359"/>
        <source>Most recent Oximetry data: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </source>
        <translation>Dados de Oximetria mais recentes: &lt;a onclick=&apos;alert(&quot;daily=%2&quot;);&apos;&gt;%1&lt;/a&gt; </translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="360"/>
        <source>(last night)</source>
        <translation>(noite passada)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="361"/>
        <source>(1 day ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="362"/>
        <source>(%2 days ago)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="367"/>
        <source>No oximetry data has been imported yet.</source>
        <translation>Nenum dado de oximetria foi importado ainda.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="40"/>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="42"/>
        <source>Contec</source>
        <translation>Contec</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.h" line="40"/>
        <source>CMS50</source>
        <translation>CMS50</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>Fisher &amp; Paykel</source>
        <translation>Fisher &amp; Paykel</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/icon_loader.h" line="78"/>
        <source>ICON</source>
        <translation>ICON</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>DeVilbiss</source>
        <translation>DeVilbiss</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="78"/>
        <source>Intellipap</source>
        <translation>Intellipap</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.h" line="86"/>
        <source>SmartFlex Settings</source>
        <translation>Configurações SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="40"/>
        <source>ChoiceMMed</source>
        <translation>ChoiceMMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/md300w1_loader.h" line="40"/>
        <source>MD300</source>
        <translation>MD300</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="67"/>
        <source>Respironics</source>
        <translation>Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/mseries_loader.h" line="67"/>
        <source>M-Series</source>
        <translation>M-Series</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>Philips Respironics</source>
        <translation>Philips Respironics</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.h" line="187"/>
        <source>System One</source>
        <translation>System One</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="92"/>
        <source>ResMed</source>
        <translation>ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="93"/>
        <source>S9</source>
        <translation>S9</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.h" line="126"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="98"/>
        <source>EPR: </source>
        <translation>EPR: </translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose</source>
        <translation>Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/somnopose_loader.h" line="39"/>
        <source>Somnopose Software</source>
        <translation>Software Somnopose</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Zeo</source>
        <translation>Zeo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/zeo_loader.h" line="40"/>
        <source>Personal Sleep Coach</source>
        <translation>Personal Sleep Coach</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="203"/>
        <source>Database Outdated
Please Rebuild CPAP Data</source>
        <translation>Banco de Dados Desatualizado
Por favor, Reconstrua os dados CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="385"/>
        <source> (%2 min, %3 sec)</source>
        <translation> (%2 min, %3 seg)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gFlagsLine.cpp" line="387"/>
        <source> (%3 sec)</source>
        <translation> (%3 seg)</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="404"/>
        <source>Pop out Graph</source>
        <translation>Deslocar Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="487"/>
        <source>The popout window is full. You should capture the existing
popout window, delete it, then pop out this graph again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1461"/>
        <source>Your machine doesn&apos;t record data to graph in Daily View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1464"/>
        <source>There is no data to graph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1610"/>
        <source>d MMM yyyy [ %1 - %2 ]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2263"/>
        <source>Hide All Events</source>
        <translation>Esconder Todos Eveitos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2264"/>
        <source>Show All Events</source>
        <translation>Mostrar Todos Eventos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2605"/>
        <source>Unpin %1 Graph</source>
        <translation>Fixar Gráfico %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2607"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2682"/>
        <source>Popout %1 Graph</source>
        <translation>Deslocar Gráfico %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2684"/>
        <source>Pin %1 Graph</source>
        <translation>Fixar Gráfico %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/MinutesAtPressure.cpp" line="831"/>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1050"/>
        <source>Plots Disabled</source>
        <translation>Desenhos Desativados</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1129"/>
        <source>Duration %1:%2:%3</source>
        <translation>Duração %1:%2:%3</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gLineChart.cpp" line="1130"/>
        <source>AHI %1</source>
        <translation>IAH %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="155"/>
        <source>Relief: %1</source>
        <translation>Alívio: %1</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="161"/>
        <source>Hours: %1h, %2m, %3s</source>
        <translation>Horas: %1h, %2m, %3s</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gdailysummary.cpp" line="264"/>
        <source>Machine Information</source>
        <translation>Informação de Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="26"/>
        <source>Journal Data</source>
        <translation>Dados de Diário</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="44"/>
        <source>OSCAR found an old Journal folder, but it looks like it&apos;s been renamed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="46"/>
        <source>OSCAR will not touch this folder, and will create a new one instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="47"/>
        <source>Please be careful when playing in OSCAR&apos;s profile folders :-P</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="54"/>
        <source>For some reason, OSCAR couldn&apos;t find a journal object record in your profile, but did find multiple Journal data folders.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="55"/>
        <source>OSCAR picked only the first one of these, and will use it in future:

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/journal.cpp" line="57"/>
        <source>If your old data is missing, copy the contents of all the other Journal_XXXXXXX folders to this one manually.</source>
        <translation>Se seus dados antigos estiverem faltando, copie o conteúdo de todas as outras pastas nomeadas Journal_XXXXXXX para esta manualmente.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="42"/>
        <source>CMS50F3.7</source>
        <translation>CMS50F3.7</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50f37_loader.h" line="42"/>
        <source>CMS50F</source>
        <translation>CMS50F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2717"/>
        <source>Backing up files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2724"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="249"/>
        <source>Reading data files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2786"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2788"/>
        <source>SmartFlex Mode</source>
        <translation>Modo SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2787"/>
        <source>Intellipap pressure relief mode.</source>
        <translation>Modo Intellipap de alívio de pressão.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2793"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="123"/>
        <source>Ramp Only</source>
        <translation>Apenas Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2794"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="124"/>
        <source>Full Time</source>
        <translation>Tempo Total</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2797"/>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2799"/>
        <source>SmartFlex Level</source>
        <translation>Nível SmartFlex</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2798"/>
        <source>Intellipap pressure relief level.</source>
        <translation>Nível de alívio de pressão Intellipap.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2805"/>
        <source>Snoring event.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/intellipap_loader.cpp" line="2806"/>
        <source>SN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="535"/>
        <source>Locating STR.edf File(s)...</source>
        <translation>Localizando arquivo(s) STR.edf...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="696"/>
        <source>Cataloguing EDF Files...</source>
        <translation>Catalogando arquivos EDF...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="715"/>
        <source>Queueing Import Tasks...</source>
        <translation>Ordenando Tarefas Importantes...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="744"/>
        <source>Finishing Up...</source>
        <translation>Terminando...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="101"/>
        <source>CPAP Mode</source>
        <translation>Modo CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="111"/>
        <source>VPAPauto</source>
        <translation>VPAPauto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="113"/>
        <source>ASVAuto</source>
        <translation>ASVAuto</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="114"/>
        <source>iVAPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="115"/>
        <source>PAC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="116"/>
        <source>Auto for Her</source>
        <translation>Auto para Ela</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="120"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1028"/>
        <source>EPR</source>
        <translation>EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="120"/>
        <source>ResMed Exhale Pressure Relief</source>
        <translation>Alívio de Pressão ResMed Exhale</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="125"/>
        <source>Patient???</source>
        <translation>Paciente???</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="128"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1034"/>
        <source>EPR Level</source>
        <translation>Nível EPR</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="128"/>
        <source>Exhale Pressure Relief Level</source>
        <translation>Alívio de Pressão de Expiração</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="135"/>
        <source>Device auto starts by breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="204"/>
        <source>Response</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="210"/>
        <source>Device auto stops by breathing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="216"/>
        <source>Patient View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="443"/>
        <source>Your ResMed CPAP device (Model %1) has not been tested yet.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="444"/>
        <source>It seems similar enough to other devices that it might work, but the developers would like a .zip copy of this device&apos;s SD card to make sure it works with OSCAR.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="135"/>
        <source>SmartStart</source>
        <translation>SmartStart</translation>
    </message>
    <message>
        <source>Machine auto starts by breathing</source>
        <translation type="vanished">Máquina liga automaticamente com a respiração</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="135"/>
        <source>Smart Start</source>
        <translation>Smart Start</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="141"/>
        <source>Humid. Status</source>
        <translation>Estado do Umidif.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="141"/>
        <source>Humidifier Enabled Status</source>
        <translation>Estado de Umidificador Ativo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2903"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Humid. Level</source>
        <translation>Nível do Umidif.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="147"/>
        <source>Humidity Level</source>
        <translation>Nível de Umidade</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="160"/>
        <source>Temperature</source>
        <translation>Temperatura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="160"/>
        <source>ClimateLine Temperature</source>
        <translation>Temperatura ClimateLine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="164"/>
        <source>Temp. Enable</source>
        <translation>Temper. Ativa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="164"/>
        <source>ClimateLine Temperature Enable</source>
        <translation>Ativar Temperatura ClimateLine</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="164"/>
        <source>Temperature Enable</source>
        <translation>Ativar Temperatura</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="171"/>
        <source>AB Filter</source>
        <translation>Filtro AB</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="171"/>
        <source>Antibacterial Filter</source>
        <translation>Filtro Antibacteriano</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="177"/>
        <source>Pt. Access</source>
        <translation>Acesso Pac.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="177"/>
        <source>Essentials</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="179"/>
        <source>Plus</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="183"/>
        <source>Climate Control</source>
        <translation>Controle Climático</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="186"/>
        <source>Manual</source>
        <translation>Manual</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="207"/>
        <source>Soft</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="206"/>
        <source>Standard</source>
        <translation type="unfinished">Padrão</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="107"/>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="110"/>
        <source>BiPAP-T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="108"/>
        <source>BiPAP-S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="109"/>
        <source>BiPAP-S/T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="210"/>
        <source>SmartStop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="210"/>
        <source>Smart Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="219"/>
        <source>Simple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="218"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="1213"/>
        <source>Parsing STR.edf records...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/common.cpp" line="855"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="2914"/>
        <location filename="../oscar/SleepLib/loader_plugins/prs1_loader.cpp" line="3005"/>
        <source>Auto</source>
        <translation>Automático</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="189"/>
        <source>Mask</source>
        <translation>Máscara</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="189"/>
        <source>ResMed Mask Setting</source>
        <translation>Configuração de Máscara ResMed</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="191"/>
        <source>Pillows</source>
        <translation>Almofadas</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="192"/>
        <source>Full Face</source>
        <translation>Facial Total</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="193"/>
        <source>Nasal</source>
        <translation>Nasal</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/resmed_loader.cpp" line="197"/>
        <source>Ramp Enable</source>
        <translation>Ativar Rampa</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="117"/>
        <source>Weinmann</source>
        <translation>Weinmann</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/weinmann_loader.h" line="117"/>
        <source>SOMNOsoft2</source>
        <translation>SOMNOsoft2</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="353"/>
        <source>Snapshot %1</source>
        <translation>Captura %1</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="268"/>
        <source>CMS50D+</source>
        <translation>CMS50D+</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/cms50_loader.cpp" line="268"/>
        <source>CMS50E/F</source>
        <translation>CMS50E/F</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="694"/>
        <source>Loading %1 data for %2...</source>
        <translation>Carregando dados %1 para %2...</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="705"/>
        <source>Scanning Files</source>
        <translation>Vasculhando arquivos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="739"/>
        <source>Migrating Summary File Location</source>
        <translation>Migrando Localização de Arquivo de Resumo</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="909"/>
        <source>Loading Summaries.xml.gz</source>
        <translation>Carregando Summaries.xml.gz</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/machine.cpp" line="1041"/>
        <source>Loading Summary Data</source>
        <translation>Carregando Dados de Resumos</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/progressdialog.cpp" line="15"/>
        <source>Please Wait...</source>
        <translation>Por favor Aguarde...</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="168"/>
        <source>Updating Statistics cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="715"/>
        <source>Usage Statistics</source>
        <translation type="unfinished">Estatísticas de Uso</translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="453"/>
        <source>%1 Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="454"/>
        <source>%1 of %2 Charts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/overview.cpp" line="729"/>
        <source>Loading summaries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/dreem_loader.h" line="37"/>
        <source>Dreem</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="93"/>
        <source>Your Viatom device generated data that OSCAR has never seen before.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.cpp" line="94"/>
        <source>The imported data may not be entirely accurate, so the developers would like a copy of your Viatom files to make sure OSCAR is handling the data correctly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/viatom_loader.h" line="40"/>
        <source>Viatom Software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="152"/>
        <source>New versions file improperly formed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="175"/>
        <source>A more recent version of OSCAR is available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>release</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="170"/>
        <source>test version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="171"/>
        <source>You are running the latest %1 of OSCAR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="172"/>
        <location filename="../oscar/checkupdates.cpp" line="176"/>
        <source>You are running OSCAR %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="178"/>
        <source>OSCAR %1 is available &lt;a href=&apos;%2&apos;&gt;here&lt;/a&gt;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="181"/>
        <source>Information about more recent test version %1 is available at &lt;a href=&apos;%2&apos;&gt;%2&lt;/a&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="209"/>
        <source>Check for OSCAR Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/checkupdates.cpp" line="276"/>
        <source>Unable to check for updates. Please try again later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1020"/>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1021"/>
        <source>SensAwake level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1028"/>
        <source>Expiratory Relief</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1034"/>
        <source>Expiratory Relief Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="1046"/>
        <source>Humidity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.h" line="88"/>
        <source>SleepStyle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/aboutdialog.cpp" line="81"/>
        <source>This page in other languages:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2607"/>
        <source>%1 Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2608"/>
        <source>%1 of %2 Graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2627"/>
        <source>%1 Event Types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/daily.cpp" line="2628"/>
        <source>%1 of %2 Event Types</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Report</name>
    <message>
        <source>about:blank</source>
        <translation type="vanished">about:blank</translation>
    </message>
</context>
<context>
    <name>SessionBar</name>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="247"/>
        <source>%1h %2m</source>
        <translation>%1: %2m {1h?}</translation>
    </message>
    <message>
        <location filename="../oscar/sessionbar.cpp" line="290"/>
        <source>No Sessions Present</source>
        <translation>Nenhuma Sessão Presente</translation>
    </message>
</context>
<context>
    <name>SleepStyleLoader</name>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="213"/>
        <source>Import Error</source>
        <translation type="unfinished">Erro de Importação</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>This device Record cannot be imported in this profile.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This Machine Record cannot be imported in this profile.</source>
        <translation type="obsolete">O relatório dessa máquina não pode ser importado nesse perfil.</translation>
    </message>
    <message>
        <location filename="../oscar/SleepLib/loader_plugins/sleepstyle_loader.cpp" line="214"/>
        <source>The Day records overlap with already existing content.</source>
        <translation type="unfinished">Os registros diários sobrepõem-se com conteúdo pré-existente.</translation>
    </message>
</context>
<context>
    <name>Statistics</name>
    <message>
        <location filename="../oscar/statistics.cpp" line="1217"/>
        <source>Details</source>
        <translation>Detalhes</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1164"/>
        <source>Most Recent</source>
        <translation>Mais Recente</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="543"/>
        <source>Therapy Efficacy</source>
        <translation>Eficácia da Terapia</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1166"/>
        <source>Last 30 Days</source>
        <translation>Últimos 30 Dias</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1168"/>
        <source>Last Year</source>
        <translation>Último Ano</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="607"/>
        <location filename="../oscar/statistics.cpp" line="608"/>
        <source>Average %1</source>
        <translation>Média %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="536"/>
        <source>CPAP Statistics</source>
        <translation>Estatísticas CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="539"/>
        <location filename="../oscar/statistics.cpp" line="1397"/>
        <source>CPAP Usage</source>
        <translation>Uso CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="540"/>
        <source>Average Hours per Night</source>
        <translation>Horas Médias por Noite</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="541"/>
        <source>Compliance (%1 hrs/day)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="556"/>
        <source>Leak Statistics</source>
        <translation>Estatísticas de Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="561"/>
        <source>Pressure Statistics</source>
        <translation>Estatísticas de Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="586"/>
        <source>Oximeter Statistics</source>
        <translation>Estatísticas de Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="590"/>
        <source>Blood Oxygen Saturation</source>
        <translation>Saturação de Oxigênio Sérico</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="595"/>
        <source>Pulse Rate</source>
        <translation>Taxa de Pulso</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="606"/>
        <source>%1 Median</source>
        <translation>Mediana %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="610"/>
        <source>Min %1</source>
        <translation>Mín %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="611"/>
        <source>Max %1</source>
        <translation>Máx %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="612"/>
        <source>%1 Index</source>
        <translation>Índice %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="613"/>
        <source>% of time in %1</source>
        <translation>% de tempo em %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="614"/>
        <source>% of time above %1 threshold</source>
        <translation>% acima do limite %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="615"/>
        <source>% of time below %1 threshold</source>
        <translation>% do tempo abaixo do limite %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="637"/>
        <source>Name: %1, %2</source>
        <translation type="unfinished">Nome: %1, %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="639"/>
        <source>DOB: %1</source>
        <translation type="unfinished">Data Nasc.: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="642"/>
        <source>Phone: %1</source>
        <translation type="unfinished">Telefone: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="645"/>
        <source>Email: %1</source>
        <translation type="unfinished">Email: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="648"/>
        <source>Address:</source>
        <translation type="unfinished">Endereço:</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="734"/>
        <source>This report was prepared on %1 by OSCAR %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="917"/>
        <source>Device Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="976"/>
        <source>Changes to Device Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1064"/>
        <source>Oscar has no data to report :(</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1398"/>
        <source>Days Used: %1</source>
        <translation type="unfinished">Dias de Uso: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1399"/>
        <source>Low Use Days: %1</source>
        <translation type="unfinished">Dias de Pouco Uso: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1400"/>
        <source>Compliance: %1%</source>
        <translation type="unfinished">Observância: %1%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1424"/>
        <source>Days AHI of 5 or greater: %1</source>
        <translation type="unfinished">Dias com IAH 5 ou mais: %1</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1431"/>
        <source>Best AHI</source>
        <translation type="unfinished">Melhor AHI</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1435"/>
        <location filename="../oscar/statistics.cpp" line="1447"/>
        <source>Date: %1 AHI: %2</source>
        <translation type="unfinished">Data: %1 IAH: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1441"/>
        <source>Worst AHI</source>
        <translation type="unfinished">Pior IAH</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1478"/>
        <source>Best Flow Limitation</source>
        <translation type="unfinished">Melhor Limitação de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1482"/>
        <location filename="../oscar/statistics.cpp" line="1495"/>
        <source>Date: %1 FL: %2</source>
        <translation type="unfinished">Data: %1 LF: %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1488"/>
        <source>Worst Flow Limtation</source>
        <translation type="unfinished">Pior Limitação de Fluxo</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1500"/>
        <source>No Flow Limitation on record</source>
        <translation type="unfinished">Nenhuma Limitação de Fluxo na gravação</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1521"/>
        <source>Worst Large Leaks</source>
        <translation type="unfinished">Pior Grande Vazamento</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1529"/>
        <source>Date: %1 Leak: %2%</source>
        <translation type="unfinished">Data: %1 Vazamento: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1535"/>
        <source>No Large Leaks on record</source>
        <translation type="unfinished">Nenhum Grande Vazamento na gravação</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1558"/>
        <source>Worst CSR</source>
        <translation type="unfinished">Pior RCS</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1566"/>
        <source>Date: %1 CSR: %2%</source>
        <translation type="unfinished">Data: %1 RCS: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1571"/>
        <source>No CSR on record</source>
        <translation type="unfinished">Nenhuma RCS na gravação</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1588"/>
        <source>Worst PB</source>
        <translation type="unfinished">Pior PR</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1596"/>
        <source>Date: %1 PB: %2%</source>
        <translation type="unfinished">Data: %1 PR: %2%</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1601"/>
        <source>No PB on record</source>
        <translation type="unfinished">Nenhum PR na gravação</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1609"/>
        <source>Want more information?</source>
        <translation type="unfinished">Quer mais informações?</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1610"/>
        <source>OSCAR needs all summary data loaded to calculate best/worst data for individual days.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1611"/>
        <source>Please enable Pre-Load Summaries checkbox in preferences to make sure this data is available.</source>
        <translation type="unfinished">Ative a caixa de seleção Pré-Carregar Dados Resumidos nas preferências para garantir que esses dados estejam disponíveis.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1631"/>
        <source>Best RX Setting</source>
        <translation type="unfinished">Melhor Configuração RX</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1634"/>
        <location filename="../oscar/statistics.cpp" line="1646"/>
        <source>Date: %1 - %2</source>
        <translation type="unfinished">Data: %1 - %2</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1637"/>
        <location filename="../oscar/statistics.cpp" line="1649"/>
        <source>AHI: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1638"/>
        <location filename="../oscar/statistics.cpp" line="1650"/>
        <source>Total Hours: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1643"/>
        <source>Worst RX Setting</source>
        <translation type="unfinished">Pior Configuração RX</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1165"/>
        <source>Last Week</source>
        <translation>Última Semana</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1062"/>
        <source>No data found?!?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1167"/>
        <source>Last 6 Months</source>
        <translation>Últimos 6 Meses</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1172"/>
        <source>Last Session</source>
        <translation>Última Sessão</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1231"/>
        <source>No %1 data available.</source>
        <translation>Nenhum dado %1 disponível.</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1234"/>
        <source>%1 day of %2 Data on %3</source>
        <translation>%1 dia de %2 dados em %3</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="1240"/>
        <source>%1 days of %2 Data, between %3 and %4</source>
        <translation>%1 dia de %2 dados em %3 e %4</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="737"/>
        <source>OSCAR is free open-source CPAP report software</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="984"/>
        <source>Days</source>
        <translation>Dias</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="988"/>
        <source>Pressure Relief</source>
        <translation>Alívio de Pressão</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="990"/>
        <source>Pressure Settings</source>
        <translation>Configurações de Pressão</translation>
    </message>
    <message>
        <source>Machine Information</source>
        <translation type="vanished">Informação de Máquina</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="923"/>
        <source>First Use</source>
        <translation>Primeiro Uso</translation>
    </message>
    <message>
        <location filename="../oscar/statistics.cpp" line="924"/>
        <source>Last Use</source>
        <translation>Último Uso</translation>
    </message>
</context>
<context>
    <name>Welcome</name>
    <message>
        <location filename="../oscar/welcome.ui" line="127"/>
        <source>Welcome to the Open Source CPAP Analysis Reporter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="142"/>
        <source>What would you like to do?</source>
        <translation>O que você gostaria de fazer?</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="185"/>
        <source>CPAP Importer</source>
        <translation>Importador CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="223"/>
        <source>Oximetry Wizard</source>
        <translation>Assistente de Oximetria</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="261"/>
        <source>Daily View</source>
        <translation>Visão Diária</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="299"/>
        <source>Overview</source>
        <translation>Visão-Geral</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="337"/>
        <source>Statistics</source>
        <translation>Estatísticas</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.ui" line="580"/>
        <source>&lt;span style=&quot; font-weight:600;&quot;&gt;Warning: &lt;/span&gt;&lt;span style=&quot; color:#ff0000;&quot;&gt;ResMed S9 SDCards need to be locked &lt;/span&gt;&lt;span style=&quot; font-weight:600; color:#ff0000;&quot;&gt;before inserting into your computer.&amp;nbsp;&amp;nbsp;&amp;nbsp;&lt;/span&gt;&lt;span style=&quot; color:#000000;&quot;&gt;&lt;br&gt;Some operating systems write index files to the card without asking, which can render your card unreadable by your cpap device.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="160"/>
        <source>It would be a good idea to check File-&gt;Preferences first,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="161"/>
        <source>as there are some options that affect import.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="162"/>
        <source>Note that some preferences are forced when a ResMed device is detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="163"/>
        <source>First import can take a few minutes.</source>
        <translation>A primeira importação pode levar alguns minutos.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="176"/>
        <source>The last time you used your %1...</source>
        <translation>A vez mais recente que você usou seu %1...</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="180"/>
        <source>last night</source>
        <translation>noite passada</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="182"/>
        <source>%2 days ago</source>
        <translation>%2 dias atrás</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="184"/>
        <source>was %1 (on %2)</source>
        <translation>foi %1 (em %2)</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="192"/>
        <source>%1 hours, %2 minutes and %3 seconds</source>
        <translation>%1 horas, %2 minutos e %3 segundos</translation>
    </message>
    <message>
        <source>Your machine was on for %1.</source>
        <translation type="vanished">Sua máquina estava ligada para %1.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="196"/>
        <source>&lt;font color = red&gt;You only had the mask on for %1.&lt;/font&gt;</source>
        <translation>&lt;font color = red&gt;Você só manteve a máscara em uso por %1.&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="209"/>
        <source>under</source>
        <translation>abaixo</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="210"/>
        <source>over</source>
        <translation>acima</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="211"/>
        <source>reasonably close to</source>
        <translation>razoavelmente próximo de</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="212"/>
        <source>equal to</source>
        <translation>igual a</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="226"/>
        <source>You had an AHI of %1, which is %2 your %3 day average of %4.</source>
        <translation>Você teve um IAH de %1, que é %2 sua média de %3 dias de %4.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="262"/>
        <source>Your pressure was under %1 %2 for %3% of the time.</source>
        <translation>Sua pressão ficou abaixo de %1 %2, %3% do tempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="285"/>
        <source>Your EPAP pressure fixed at %1 %2.</source>
        <translation>Sua pressão EPAP fixou em %1 %2.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="288"/>
        <location filename="../oscar/welcome.cpp" line="297"/>
        <source>Your IPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Sua pressão IPAP ficou abaixo de %1 %2, %3% do tempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="296"/>
        <source>Your EPAP pressure was under %1 %2 for %3% of the time.</source>
        <translation>Sua pressão EPAP ficou abaixo de %1 %2, %3% do tempo.</translation>
    </message>
    <message>
        <source>Your machine was under %1-%2 %3 for %4% of the time.</source>
        <translation type="vanished">Sua máquina ficou abaixo de %1-%2 %3, %4% do tempo.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="181"/>
        <source>1 day ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="195"/>
        <source>Your device was on for %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="257"/>
        <source>Your CPAP device used a constant %1 %2 of air</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="270"/>
        <source>Your device used a constant %1-%2 %3 of air.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="277"/>
        <source>Your device was under %1-%2 %3 for %4% of the time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="317"/>
        <source>Your average leaks were %1 %2, which is %3 your %4 day average of %5.</source>
        <translation>Seus vazamentos médios foram %1 %2, que é %3 sua média de %4 dias de %5.</translation>
    </message>
    <message>
        <location filename="../oscar/welcome.cpp" line="323"/>
        <source>No CPAP data has been imported yet.</source>
        <translation>Nenhum dado CPAP foi importado ainda.</translation>
    </message>
</context>
<context>
    <name>gGraph</name>
    <message>
        <location filename="../oscar/Graphs/gGraph.cpp" line="796"/>
        <source>%1 days</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>gGraphView</name>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="409"/>
        <source>100% zoom level</source>
        <translation>100% de aproximação</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="411"/>
        <source>Restore X-axis zoom to 100% to view entire selected period.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="413"/>
        <source>Restore X-axis zoom to 100% to view entire day&apos;s data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="415"/>
        <source>Reset Graph Layout</source>
        <translation>Redefinir Disposição de Gráfico</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="416"/>
        <source>Resets all graphs to a uniform height and default order.</source>
        <translation>Redefine todos os gráficos para altura uniforme e ordenação padrão.</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="419"/>
        <source>Y-Axis</source>
        <translation>EixoY</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="420"/>
        <source>Plots</source>
        <translation>Desenhos</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="425"/>
        <source>CPAP Overlays</source>
        <translation>Sobreposições CPAP</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="428"/>
        <source>Oximeter Overlays</source>
        <translation>Sobreposições Oxímetro</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="431"/>
        <source>Dotted Lines</source>
        <translation>Linhas Pontilhadas</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1803"/>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="1856"/>
        <source>Double click title to pin / unpin
Click and drag to reorder graphs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2101"/>
        <source>Remove Clone</source>
        <translation>Remover Clone</translation>
    </message>
    <message>
        <location filename="../oscar/Graphs/gGraphView.cpp" line="2105"/>
        <source>Clone %1 Graph</source>
        <translation>Clonar Gráfico %1</translation>
    </message>
</context>
</TS>
